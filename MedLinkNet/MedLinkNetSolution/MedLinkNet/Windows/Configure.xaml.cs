﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MedLinkNet.Model.Connection;
using MCMEDLink.Controls;

namespace MCMEDLink.Windows
{
    /// <summary>
    /// Interaction logic for Configure.xaml
    /// </summary>
    public partial class Configure : Window
    {
        public Configure()
        {
            InitializeComponent();
        }

        private string admintx;

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
          //  username.Content = Properties.Settings.Default.UserName;
          //  ubicacion.Content = Properties.Settings.Default.Ubicacion;
          //  fecha.Content = Properties.Settings.Default.InstallDate;
          //  idinstall.Content = Properties.Settings.Default.InstallID;
            //
            this.ExamPath.Text = Properties.Settings.Default.ExamPath;
            this.ReportPath.Text = Properties.Settings.Default.ReportPath;
            this.ExamPath2.Text = Properties.Settings.Default.ExamPath2;
            this.ReportPath2.Text = Properties.Settings.Default.ReportPath2;
            this.ExamPath3.Text = Properties.Settings.Default.ExamPath3;
            this.ReportPath3.Text = Properties.Settings.Default.ReportPath3;
            //
            // Take the elements for combo boxes
            DataAccessLayer dl = new DataAccessLayer();        
            List<string> TestNames = dl.GetAllTestThisUser(Properties.Settings.Default.UserName,Properties.Settings.Default.Passw);
            foreach (string elem in TestNames)
            {
                this.comboBox1.Items.Add(elem);
                this.comboBox2.Items.Add(elem);
                this.comboBox3.Items.Add(elem);           
            }           
            //
            this.comboBox1.SelectedItem = Properties.Settings.Default.ExamID;
            this.comboBox2.SelectedItem = Properties.Settings.Default.ExamID2;
            this.comboBox3.SelectedItem = Properties.Settings.Default.ExamID3;
            //
            adminTb.Text = "VERSION "+System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            admintx=adminTb.Text;
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.ExamPath = this.ExamPath.Text;
            Properties.Settings.Default.ReportPath = this.ReportPath.Text;
            Properties.Settings.Default.ExamPath2 = this.ExamPath2.Text;
            Properties.Settings.Default.ReportPath2 = this.ReportPath2.Text;
            Properties.Settings.Default.ExamPath3 = this.ExamPath3.Text;
            Properties.Settings.Default.ReportPath3 = this.ReportPath3.Text;
            //
            Properties.Settings.Default.ExamID = (string)this.comboBox1.SelectedItem;
            Properties.Settings.Default.ExamID2 = (string)this.comboBox2.SelectedItem;
            Properties.Settings.Default.ExamID3 = (string)this.comboBox3.SelectedItem;
            //
            Properties.Settings.Default.Save(); // Save for permanent change
            adminTb.Text=admintx;         
        }

        private void OkBtnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Properties.Settings.Default.Save();
            Utils.SaveInstallationLocalData();
        }



        private void DesintalarBn_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
          try
          {

            string userFilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string userFilesGoHere = System.IO.Path.Combine(userFilePath, "/pcopy");

            string FilePath = System.IO.Path.Combine(userFilesGoHere, "Local2.dat");

            if (File.Exists(FilePath)) File.Delete(FilePath); // borrar local trace

            Properties.Settings.Default.Activated = false;
            Application.Current.Shutdown();
          }
          catch
          {

          }

        }
  }
}
