﻿using MCMEDLink.Controls;
using MedLinkNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MedLinkNet.Model.Connection;

namespace MCMEDLink.Windows
{
  /// <summary>
  /// Interaction logic for Window1.xaml
  /// </summary>
  public partial class InstallEdit : Window
  {
    private bool newinstall;

    DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
    Installation thisInstall;

    List<string> TipoListItems = new List<string> { };


    public InstallEdit(Installation install, bool nuevo)
    {
      InitializeComponent();

      install.ActKey = install.ActKey.ToUpper(); // garantizar en mayusculas;

      thisInstall = install; // guardar lo inicial
      newinstall = nuevo;

      if (thisInstall != null)
      {

        List<Role> TList = dataAccessLayer.GetRoleList();
        TipoCb.ItemsSource = TList;

        foreach (Role item in TList) TipoListItems.Add(item.ToString()); 

        Show(thisInstall);
      }
      
    }

    private void Show(Installation View)
    { 
      
        if (!newinstall) IDLb.Content = View.IDInstall; else IDLb.Content = "NUEVO";

        TestLb.Content = dataAccessLayer.GetTestName(View.TestID);

        UserBox.Text = View.UserName;    
        PasswBox.Text = View.Passw;   
        UbicacionBx.Text = View.Ubicacion;

        ActKeyBox.Text = View.ActKey; 

          UserBox.IsReadOnly = true;
          PasswBox.IsReadOnly = true;

        string item = dataAccessLayer.GetRole(View.UserType).ToString(); 
        TipoCb.SelectedIndex = TipoListItems.IndexOf(item);

        InstalledCB.IsChecked = View.Installed;
       
        FechaInstallBx.Text = View.InstallDate.ToString();

        NserieBx.Text = View.DeviceSerial;
        MachineBx.Text = View.MachineName;

        CodeBx.Text = View.CodeID.ToString();

    }

    private void UpdatethisInstall()
    {
      thisInstall.UserName = UserBox.Text;
      thisInstall.Passw = PasswBox.Text;

      thisInstall.Ubicacion = UbicacionBx.Text;

      if (TipoCb.SelectedIndex>=0) thisInstall.UserType = ((Role)TipoCb.SelectedItem).IDROLE;
      thisInstall.Installed = (bool)InstalledCB.IsChecked;

      thisInstall.DeviceSerial = NserieBx.Text;
      thisInstall.MachineName = MachineBx.Text;

      try { thisInstall.CodeID = Convert.ToInt16(CodeBx.Text); } catch { thisInstall.CodeID = 0; }
    }

    private void CancelBn_Click(object sender, RoutedEventArgs e)
    {
      Show(thisInstall); // de nuevo el inicial
    }

    private void SaveBn_Click(object sender, RoutedEventArgs e)
    {
      // actualizar datos desde la forma

      UpdatethisInstall();

      if (newinstall)  // Update / Insert User
      {
        try
        {
          dataAccessLayer.CreateNewInstall(thisInstall);
        }
        catch (Exception ex)
        {
          string mensaje = ex.Message;
        }

      }
      else
      {
        try
        {
          dataAccessLayer.UpdateInstall(thisInstall.IDInstall, thisInstall);
        }
        catch (Exception ex)
        {
          string mensaje = ex.Message;
        }
      }

      Close();
    }

  }
}
