﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading;
using System.ComponentModel;
using System.IO;
using MedLinkNet.IOAccess;

namespace MCMEDLink.Windows
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class CopyWindow : Window
    {
        public CopyWindow()
        {
            InitializeComponent();
        }

        string FromFolder;            // Can content Folder or File Path
        string DestinationFolder;     // 
        bool FileType;                // Indicate if it only a file and not a folder 
        bool Upload;                  // Indicate its an Upload if not it is a DownLoad for AWS operation
        bool ProcessResult;           // false if operation not complete or exception occur else true
        MedLinkNetClient client;      // AWSClient Object
        Exception OpException;        // Exception if ProcessResult = false;

        BackgroundWorker BackProcess; // To run Copy Process in another Thread
        static AutoResetEvent WaitEvent = new AutoResetEvent(false); // Use to notify Copy Progression
        bool running = false;

        void CopyProcess()
        {
            running = true;

                if (Properties.Settings.Default.JDEnabled == true) // CASE OF FTP
                {
                    try
                    {
                        FTPFunctions FTP = new FTPFunctions();

                        if (FileType == true) FTP.Upload(FromFolder); //  Upload a simple File             
                            
                        ProcessResult = true;
                    }

                    catch (Exception ex)
                    {
                        ProcessResult = false;
                        OpException = ex;
                    }
                   
                }

                else // CASE OF AWS
                {
                    try
                    {  
                        // Prepare AWS Connection

                        client = new MedLinkNetClient();
                        client.ApplyCompress = true;  // Activate Compress 
                    //    client.OnProgress += client_OnProgress;

                        if (this.Upload == true)   // AWS UPLOAD CASE
                        {
                            if (FileType == true)   // ONLY A FILE
                            {
                                FileInfo file = new FileInfo(FromFolder);
                                string keyName = DestinationFolder + "/" + file.Name;
                                string sourceFile = FromFolder; // in this case mean full File name
                                if (file.Length > 0) client.TransferFile(keyName, sourceFile);
                            }
                            else // Put the hole directory including subdirectories
                            {
                                client.TransferFolder(DestinationFolder, FromFolder, true);
                            }
                        }
                        else // AWS DOWNLOAD CASE
                        {
                            // DOWNLOAD A DIRECTORY CONTENT FROM AWS
                            client.GetFolder(FromFolder, DestinationFolder);
                        }

                        ProcessResult = true;
                    }

                    catch (Exception  ex)
                    {
                        ProcessResult = false;
                        OpException = ex;
                    }
                }
                running = false;
            }

        int min = 0;
        int seg = 0;
        int tic = 0;

        void Update(long TransBytes, long TBytes, double TMb, string Trace)
        {
            if (TBytes > 0)
            {
                PB.Value = 100 * TransBytes / TBytes;
                double n = (TMb + TransBytes) / 1000000;
                if (n > 0) LMb.Content = n.ToString("F1") + " Mb";
            }
            TraceLb.Content = Trace;
            TimerLb.Content = min.ToString() + ":" + seg.ToString("00");
            Status.Flash();
        }


        public bool StartCopy(string fromFolder, string destinationFolder, bool fileType, bool upload)
        {
            bool resultado=false;
   
            this.Show();

            try
            {
                FromFolder = fromFolder;
                FileType = fileType;
                //
                // include the last fromFolder name in the Destination IF UPLOADING FROM A SUBDIRECTORY
                if ((Upload == true) & (FileType == false))
                {
                  string[] dirlist = fromFolder.Split('\\');
                  int n = dirlist.Count();
                  if (n > 1)
                  {
                    int last = n - 1;
                    if (dirlist[last] != "") destinationFolder  = destinationFolder + "/" + dirlist[last]; 
                  }  
                 }

                DestinationFolder = destinationFolder;
                Upload = upload;

                if (Upload == true) OperationLb.Content = "Enviando ..";
                else OperationLb.Content = "Recibiendo ..";
                Status.Flash();

                BackProcess = new BackgroundWorker();
                BackProcess.DoWork+=new DoWorkEventHandler(BackProcess_DoWork);
                BackProcess.RunWorkerCompleted+=new RunWorkerCompletedEventHandler(BackProcess_RunWorkerCompleted);
                BackProcess.RunWorkerAsync();

                min = 0; seg = 0; tic = 0;
                do
                {
                    System.Threading.Thread.Sleep(500);
                    tic = tic + 500;
                    if (tic == 1000)
                    {
                        tic = 0;
                        seg = seg + 1;
                        if (seg == 60)
                        {
                            seg = 0;
                            min = min + 1;
                        }
                    }

                    if (Properties.Settings.Default.JDEnabled == false) // CASE OF AWS
                    {
                        Update(client.TransferredBytes, client.TotalBytes, client.AcumMb, client.trace);
                    }
                    Status.Flash();
                } 
                while (running);         
            }

            finally
            {
                resultado = ProcessResult;
                if (!resultado)
                {
                    MessageBox.Show(OpException.Message, "Operacion no completada !", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return resultado;  
        }

        void client_OnProgress(object sender, EventArgs e)
        {
            WaitEvent.Set();
        }           

        void BackProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WaitEvent.Set();
        }

        void BackProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            CopyProcess(); // Start CopyProcess
        }

    }
}
