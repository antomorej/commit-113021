﻿using System;
using System.Windows;
using System.Collections.Generic;
using MedLinkNet.Model;
using MedLinkNet.Model.Connection;
using MCMEDLink.Controls;


namespace MCMEDLink.Windows
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {

        private bool userchange = false;

        public Login()
        {
            InitializeComponent();
            VersionLb.Content = "V " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        
        }

        public bool HasUserExited { get; private set; }
        public User LoggedInUser { get; private set; }

        private void LoginBtnClick(object sender, RoutedEventArgs e)
        {
            AuthenticateUser();
        }

        private void AuthenticateUser()
        {
            try
            {
                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                WorkGroup grupoid = (WorkGroup)workGroupBx.SelectedItem;
                LoggedInUser = dataAccessLayer.AuthenticateUser(grupoid.IdWorkGroup, this.usernameTb.Text, this.passwordPb.Password);
                if (LoggedInUser != null)
                {
                  Properties.Settings.Default.UserName= this.usernameTb.Text ;
                  Properties.Settings.Default.Passw = this.passwordPb.Password;
                  Properties.Settings.Default.Save();
                  Utils.SaveInstallationLocalData(); // save track info
                }

                this.DialogResult = LoggedInUser != null;
            }
            catch (Exception errorConnecting)
            {
                MessageBox.Show(errorConnecting.Message, "Revise Conexion a Internet", MessageBoxButton.OK, MessageBoxImage.Error);
                this.DialogResult = null;
            }
        }

        private void ExitBtnClick(object sender, RoutedEventArgs e)
        {
            HasUserExited = true;
            this.DialogResult = false;
        }

        private void FindWorkGroups()
        {
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            List<WorkGroup > groupList = dataAccessLayer.GetAllWorkGroupsThisUser(this.usernameTb.Text, this.passwordPb.Password);
            workGroupBx.ItemsSource = groupList;
            if (groupList.Count > 0)
            {
                workGroupBx.SelectedValuePath = "IdWorkGroup";
            }
        }

        private void workGroupBx_DropDownOpened(object sender, EventArgs e)
        {
            FindWorkGroups();
        }

        private void Start()
        {
                try
                {
                    MessageLb.Content = "Connecting...";
                    StatusLedPanel.State(2, 500);
                    FindWorkGroups();  // try to do something to test connection
                    StatusLedPanel.State(3, 500);
                    MessageLb.Content = "Seleccione una Red";
                    //
                    // Get the user data from Localdata firts if exist to override settings or from Settings
                    //
                    Utils.ReadInstallationLocalProperties();

                    if (!Properties.Settings.Default.Activated) // Sono esta activado empezar en blanco
                    {
                      Properties.Settings.Default.UserName = "";
                      Properties.Settings.Default.Passw = "";
                    }
      
                    
                      usernameTb.Text = Properties.Settings.Default.UserName;
                      string passw = Properties.Settings.Default.Passw;
                      passwordPb.Password = passw;
                   

                    // permitir edicion Si es SUPERADMIN sino quedarse con los datos del primer login

                    if ((passw == "") || (userchange ))
                    {
                        passwordPb.IsHitTestVisible = true;
                        usernameTb.IsReadOnly = false;
                    }
                     else
                    {
                         passwordPb.IsHitTestVisible = false;
                         usernameTb.IsReadOnly = true;
                    }

                    workGroupBx.Focus();
                }
                catch
                {
                    loginBtn.IsEnabled = false;
                    MessageLb.Content = "Revise Accesso a Internet";  
                    StatusLedPanel.State(1, 500);
                    reConnect.Visibility = Visibility.Visible;
                }
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            Start();
        }

        private void workGroupBx_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            loginBtn.IsEnabled = (workGroupBx.Items.Count > 0);
        }

        private void reConnect_Click(object sender, RoutedEventArgs e)
        {
            reConnect.Visibility = Visibility.Hidden ;
            Start();
        }

        private void image1_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.mediventa.com");
        }

        private void VersionLb_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
          userchange = true; // PERMITIR EL CAMBIO DE USUARIOS !!!!!
          Start();
        }

    
      }
    }
