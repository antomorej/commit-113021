﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MedLinkNet.Model;
using MedLinkNet.Model.Connection;


namespace MCMEDLink.Windows
{
    /// <summary>
    /// Interaction logic for PatientView.xaml
    /// </summary>
    public partial class PatientView : Window
    {
        public PatientView(int PID)
        {
            InitializeComponent();

            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            PatientData SelectedPatient = dataAccessLayer.GetPatientData(PID);

            if (SelectedPatient != null)
            {
                this.Show();
                //
                FolioTb.Text = SelectedPatient.PCode;
                NombreTb.Text = SelectedPatient.PName;
                EdadTb.Text = SelectedPatient.Edad;
                SexoTb.Text = SelectedPatient.Sexo;
                obsTb.Text = SelectedPatient.Obs;
            }

        }


        private void Window_Deactivated(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
