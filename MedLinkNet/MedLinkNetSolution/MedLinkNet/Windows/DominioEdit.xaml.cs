﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MedLinkNet.Model;
using MedLinkNet.Model.Connection;
using MedLinkNet.IOAccess;


namespace MCMEDLink.Windows
{
  
  /// </summary>
  public partial class DominioEdit : Window
  {

    DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();

    Domain SelectedDom;

    WorkGroup SelectedGroup;

    List<string> TestList = new List<string> { };


    public DominioEdit()
    {
      InitializeComponent();
     
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      List <Test> tests= dataAccessLayer.GetAllTestNames();

      TestList.Clear();
      foreach (Test item in tests) { TestList.Add(item.ToString()); }

      TestCb.ItemsSource = tests;
      TestCb.SelectedIndex = 0;

      Refresh("newlist");

    }

    public void SelectPos(DataGrid Grid, int pos)
    {
      if (Grid.HasItems)
      {
        Grid.SelectedIndex = pos; 
        DomGrid.ScrollIntoView(Grid.SelectedItem);
      }
      Grid.Focus();
    }
    private void Refresh(string cmd)
    {
      int LastSelect = DomGrid.SelectedIndex;  // guardar posicion actual

      if (cmd =="newlist") LastSelect = 0;   // primera posicion si es newlist


      List<Domain> DomainList = dataAccessLayer.GetDomainsList();

      if (DomainList.Count > 0)
      {
        DomGrid.ItemsSource = null;
        DomGrid.ItemsSource = DomainList;

        int n = DomGrid.Items.Count - 1;
        if ( (LastSelect > n) || (cmd =="newitem")) LastSelect = n;

        if (LastSelect >= 0) SelectPos(DomGrid, LastSelect);  // Recovery position
        else SelectPos(DomGrid,0);

      }
      else
      {
        DomGrid.ItemsSource = null;
      }

      ShowStatus();
    }

    private void RefreshInstall(string cmd)
    {
      //mostrar las instalaciones de este user
      List<WorkGroup> GrupoList = dataAccessLayer.GetAllWorkGroupsThisDomain(SelectedDom.IDDOMINIO);

      int LastSelect = GrupoGrid.SelectedIndex;
      if (cmd == "newlist") LastSelect = 0;   // primera posicion si es newlist
      
      if (GrupoList.Count > 0)
      {
        GrupoGrid.ItemsSource = null;
        GrupoGrid.ItemsSource = GrupoList;

        int n = GrupoGrid.Items.Count - 1;
        if ((LastSelect > n) || (cmd == "newitem")) LastSelect = n;

        if (LastSelect >= 0) SelectPos(GrupoGrid, LastSelect);  // Recovery position
        else SelectPos(DomGrid, 0);

      }
      else
      {
        GrupoGrid.ItemsSource = null;
      }

      ShowInstallStatus();

    }

    private void ShowStatus()
    {

      SelectedDom = (Domain)DomGrid.SelectedItem;

      if (SelectedDom != null)
      {
        DOMBX.Text = SelectedDom.DOMINIONAME;
        DESCBX.Text = SelectedDom.DESCRIPTION;
        ALIASBX.Text = SelectedDom.ALIAS;

        EditBn.IsEnabled = true;
        DeleteBn.IsEnabled = true;

        RefreshInstall("newlist");
      }
      else
      {
        DOMBX.Text = "";
        DESCBX.Text = "";
        ALIASBX.Text = "";

        EditBn.IsEnabled = false;
        DeleteBn.IsEnabled = false;
        GrupoGrid.ItemsSource = null;
      }
    }

    private void DomGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ShowStatus();
    }


    private void NewBn_Click(object sender, RoutedEventArgs e)
    {
      Domain newdom = new Domain { DOMINIONAME = DOMBX.Text, DESCRIPTION = DESCBX.Text, ALIAS = ALIASBX.Text };

      try
      {
        dataAccessLayer.CreateNewDomain(newdom);
      }
      catch (Exception Ex)
      {
        MessageBox.Show(Ex.Message);
      }

      Refresh("newitem");
    }

    private void EditBn_Click(object sender, RoutedEventArgs e)
    {
      SelectedDom.DOMINIONAME = DOMBX.Text;
      SelectedDom.DESCRIPTION = DESCBX.Text;
      SelectedDom.ALIAS = ALIASBX.Text;

      try
      {
        dataAccessLayer.UpdateDomain(SelectedDom);
      }
      catch (Exception Ex)
      {
        MessageBox.Show(Ex.Message);
      }

      Refresh(null);
    }

    private void DeleteBn_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      if (SelectedDom != null)
      {
        try
        {
          dataAccessLayer.DeleteDomain(SelectedDom.IDDOMINIO);
        }
        catch (Exception Ex)
        {
          MessageBox.Show(Ex.Message);
        }

        Refresh(null);

      }
    
    }


    private void ShowInstallStatus()
    {
      SelectedGroup = (WorkGroup)GrupoGrid.SelectedItem;

      if (SelectedGroup != null)
      { 
        String item = dataAccessLayer.GetTest(SelectedGroup.IdTest).ToString();
        TestCb.SelectedIndex = TestList.IndexOf(item);
        GRUPOBOX.Text = SelectedGroup.GroupName;
        EditInstBn.IsEnabled = true;
        DeleteInstBn.IsEnabled = true;
      }
      else
      {
        GRUPOBOX.Text = "";
        EditInstBn.IsEnabled = false;
        DeleteInstBn.IsEnabled = false;
      }
    }
    private void GrupoGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ShowInstallStatus();
    }


    private void NewInstBn_Click(object sender, RoutedEventArgs e)
    {
      WorkGroup newgroup = new WorkGroup { GroupName = GRUPOBOX.Text, IdDominio= ((Domain)SelectedDom).IDDOMINIO, IdTest = ((Test)TestCb.SelectedItem).TESTID };
 
      try
      {
        dataAccessLayer.CreateNewWorkGroup(newgroup);
      }
      catch (Exception Ex)
      {
        MessageBox.Show(Ex.Message);
      }


      RefreshInstall("newitem");
    }

    private void EditInstBn_Click(object sender, RoutedEventArgs e)
    {

      SelectedGroup.GroupName = GRUPOBOX.Text;
      SelectedGroup.IdTest = ((Test)TestCb.SelectedItem).TESTID;

      try
      {
        dataAccessLayer.UpdateWorkGroup(SelectedGroup);
      }
      catch (Exception Ex)
      {
        MessageBox.Show(Ex.Message);
      }

      RefreshInstall(null);

    }

    private void DeleteInstBn_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      if (SelectedGroup != null)
      {

        try
        {
          dataAccessLayer.DeleteWorkGroup(SelectedGroup.IdWorkGroup);
        }
        catch (Exception Ex)
        {
          MessageBox.Show(Ex.Message);
        }
      }

      RefreshInstall(null);

    }

 
  }
}
