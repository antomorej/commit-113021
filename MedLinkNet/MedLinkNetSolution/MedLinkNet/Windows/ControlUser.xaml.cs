﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MedLinkNet.Model;
using MedLinkNet.Model.Connection;
using MedLinkNet.IOAccess;


namespace MCMEDLink.Windows
{
  /// <summary>
  /// Interaction logic for ControlUser.xaml
  /// </summary>
  public partial class ControlUser : Window
  {

    DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();

    UserView SelectedUser;

    Installation SelectedLic;


    public ControlUser()
    {
      InitializeComponent();
     
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      List<Domain> DomainList = dataAccessLayer.GetDomainsList();

      Domain Todos = new Domain { IDDOMINIO = 0, DOMINIONAME = "TODOS" };
      DomainList.Add(Todos);

      DominioCb.ItemsSource = DomainList;
      DominioCb.SelectedIndex = 0; // seleccionar el primero y provooocar update
    }
 

    public void SelectPos(DataGrid Grid, int pos)
    {
      if (Grid.HasItems)
      {
        Grid.SelectedIndex = pos; 
        UserGrid.ScrollIntoView(Grid.SelectedItem);
      }
      Grid.Focus();
    }
    private void Refresh(string cmd)
    {
      int LastSelect = UserGrid.SelectedIndex;  // guardar posicion actual

      if (cmd =="newlist") LastSelect = 0;   // primera posicion si es newlist

      int DID = 0;
      if (DominioCb.SelectedItem != null) DID = ((Domain)DominioCb.SelectedItem).IDDOMINIO;
      int GID = 0;
      if (GrupoCb.SelectedItem != null) GID = ((WorkGroup)GrupoCb.SelectedItem).IdWorkGroup;

      List<UserView> UserList = dataAccessLayer.GetAllUserView(DID, GID);

      if (UserList.Count > 0)
      {
        UserGrid.ItemsSource = null;
        UserGrid.ItemsSource = UserList;

        int n = UserGrid.Items.Count - 1;
        if ( (LastSelect > n) || (cmd =="newitem")) LastSelect = n;

        if (LastSelect >= 0) SelectPos(UserGrid, LastSelect);  // Recovery position
        else SelectPos(UserGrid,0);

        Nlabel.Content = (n + 1).ToString();

      }
      else
      {
        UserGrid.ItemsSource = null;
      }

      ShowStatus();
    }

    private void RefreshInstall(string cmd)
    {
      //mostrar las instalaciones de este user
      List<Installation> Installs = dataAccessLayer.GetInstallationsthisUser(SelectedUser.Username, SelectedUser.Password, SelectedUser.TestID);

      int LastSelect = InstGrid.SelectedIndex;
      if (cmd == "newlist") LastSelect = 0;   // primera posicion si es newlist
      
      if (Installs.Count > 0)
      {
        InstGrid.ItemsSource = null;
        InstGrid.ItemsSource = Installs;

        int n = InstGrid.Items.Count - 1;
        if ((LastSelect > n) || (cmd == "newitem")) LastSelect = n;

        if (LastSelect >= 0) SelectPos(InstGrid, LastSelect);  // Recovery position
        else SelectPos(UserGrid, 0);

      }
      else
      {
        InstGrid.ItemsSource = null;
      }

      ShowInstallStatus();

    }

    private void ShowStatus()
    {
      if (dataAccessLayer.error) StatusBox.Text = "Error"; else StatusBox.Text = "OK";
      if (dataAccessLayer.error) StatusBox.Text = StatusBox.Text + " " + dataAccessLayer.Emensaje;

      SelectedUser = (UserView)UserGrid.SelectedItem;

      if (SelectedUser != null)
      {
        IDBOX.Text = SelectedUser.Name + " - " + SelectedUser.Username +  " - " + SelectedUser.Password + " - " + SelectedUser.NSerie;
        EditBn.IsEnabled = true;
        DeleteBn.IsEnabled = true;
        UserEnableCB.IsEnabled = true;

        UserEnableCB.IsChecked = SelectedUser.Enabled;

        RefreshInstall("newlist");
      }
      else
      {
        IDBOX.Text = "";
        EditBn.IsEnabled = false;
        DeleteBn.IsEnabled = false;
        UserEnableCB.IsEnabled = false;
        InstGrid.ItemsSource = null;
      }
    }

    private void ShowStatus(string mensaje)
    {
       StatusBox.Text = mensaje;
    }

    private void UserGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ShowStatus();
    }

    private void DominioCb_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      int DID = 0;
      if (DominioCb.SelectedItem != null) DID = ((Domain)DominioCb.SelectedItem).IDDOMINIO;
      List<WorkGroup> GroupList = dataAccessLayer.GetAllWorkGroupsThisDomain(DID);

      ShowStatus(" Wait ... ");

      WorkGroup  Todos = new WorkGroup { IdWorkGroup = 0, GroupName = "TODOS" };
      GroupList.Add(Todos);
      GrupoCb.ItemsSource = null;
      GrupoCb.ItemsSource = GroupList;
      GrupoCb.SelectedIndex = 0; // seleccionar primero y provocar GrupoChange
    }

    private void GrupoCb_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      int lastpos = GrupoCb.Items.Count - 1;
      if (GrupoCb.SelectedIndex == lastpos) NewBn.IsEnabled = false; else NewBn.IsEnabled = true;
      Refresh("newlist");
    }

    private void UserGrid_LoadingRow(object sender, DataGridRowEventArgs e)
    {
      if (UserGrid.Columns.Count() > 0)
      {
        UserGrid.Columns[1].Visibility = Visibility.Hidden;
        UserGrid.Columns[2].Visibility = Visibility.Hidden;
        UserGrid.Columns[3].Visibility = Visibility.Hidden;
        UserGrid.Columns[4].Visibility = Visibility.Hidden;
        UserGrid.Columns[5].Visibility = Visibility.Hidden;

        UserGrid.Columns[7].Visibility = Visibility.Hidden;
        UserGrid.Columns[13].Visibility = Visibility.Hidden;
        UserGrid.Columns[16].Visibility = Visibility.Hidden;
        UserGrid.Columns[17].Visibility = Visibility.Hidden;

        for (int i = 19; i <= 26; i++) UserGrid.Columns[i].Visibility = Visibility.Hidden;
      }
      
    }


    private void NewBn_Click(object sender, RoutedEventArgs e)
    {   
      UserView newuser = new UserView { GrupoID = ((WorkGroup)GrupoCb.SelectedItem).IdWorkGroup, TestID = ((WorkGroup)GrupoCb.SelectedItem).IdTest, UsertypeID = 3, Enabled = true, FechaInstall = DateTime.Today.ToString(), Fechapoliza = (DateTime.Today).ToString()} ;
      UserEdit windows = new UserEdit(newuser,true); 
      windows.ShowDialog();
      Refresh("newitem");
    }

    private void EditBn_Click(object sender, RoutedEventArgs e)
    {
      if (SelectedUser != null)
      {
        try
        {
          UserView thisUserView = dataAccessLayer.GetUserViewData(SelectedUser.UserID); // Traer todo el record
          UserEdit panel = new UserEdit(thisUserView, false);
          panel.ShowDialog();
          Refresh(null);
        }
        catch (Exception Ex)
        {
          ShowStatus(Ex.Message);
        }
      }
    }

    private void DeleteBn_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      if ( (SelectedUser != null) & (InstGrid.Items.Count == 0) )
      {
        ShowStatus("Borrando...");
        try
        {
          dataAccessLayer.DeleteUser(SelectedUser.UserID,SelectedUser.Username,SelectedUser.Password);
          Refresh(null);
        }
        catch (Exception Ex)
        {
          ShowStatus(Ex.Message);
        }
      }
      else ShowStatus("Usuario con Instalaciones !!");
    }


    private void ShowInstallStatus()
    {
      SelectedLic = (Installation)InstGrid.SelectedItem;

      if (SelectedLic != null)
      {

        LICBOX.Text = SelectedLic.UserName + " - " + SelectedLic.Passw+ " - " + SelectedLic.ActKey;
        EditInstBn.IsEnabled = true;
        DeleteInstBn.IsEnabled = true;
      }
      else
      {
        LICBOX.Text = "";
        EditInstBn.IsEnabled = false;
        DeleteInstBn.IsEnabled = false;
      }
    }
    private void InstGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ShowInstallStatus();
    }

    private void UserEnableCB_Click(object sender, RoutedEventArgs e)
    {
      bool status = (bool)UserEnableCB.IsChecked;
      if (SelectedUser.Enabled != status) // si hay cambio de status
      { 
          dataAccessLayer.ChangeUserEnabled(SelectedUser.UserID, status);
          Refresh(null);
      }
    }

    private void NewInstBn_Click(object sender, RoutedEventArgs e)
    {
      Installation  newinst = new Installation { UserName = ((UserView)SelectedUser).Username, Passw = ((UserView)SelectedUser).Password, ActKey = Guid.NewGuid().ToString().ToUpper(), InstallDate = DateTime.Now, TestID = ((WorkGroup)GrupoCb.SelectedItem).IdTest, UserType = 3, Installed = false, DeviceSerial ="DEMO-0000", CodeID=0 };
      InstallEdit windows = new InstallEdit(newinst, true);
      windows.ShowDialog();

      RefreshInstall("newitem");
    }

    private void EditInstBn_Click(object sender, RoutedEventArgs e)
    {
      if (SelectedLic != null)
      { 
        InstallEdit windows = new InstallEdit(SelectedLic, false);
        windows.ShowDialog();
        RefreshInstall(null);
      }

    }

    private void DeleteInstBn_Click(object sender, RoutedEventArgs e)
    {
      ShowStatus("Use Doble Click si realmenrte desea borrar esta LICENCIA !!");
    }

    private void DeleteBn_Click(object sender, RoutedEventArgs e)
    {
      ShowStatus("Use Doble Click si realmenrte desea borrar el USUARIO !!");
    }

    private void DeleteInstBn_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      if (SelectedLic != null)
      {
        ShowStatus("Borrando...");
        try
        {
          dataAccessLayer.DeleteInstallation(SelectedLic.IDInstall);
          Refresh(null);
        }
        catch (Exception Ex)
        {
          ShowStatus(Ex.Message);
        }
      }
      else ShowStatus("Seleccione Licencia");
     
    }

    private void EditDomBn_Click(object sender, RoutedEventArgs e)
    {
      DominioEdit windows = new DominioEdit();
      windows.ShowDialog();


    }
  }
}
