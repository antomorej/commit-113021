﻿using System;
using System.Windows;
using System.Collections.Generic;
using MedLinkNet.Model;
using MedLinkNet.Model.Connection;

namespace MCMEDLink.Windows
{
    /// <summary>
    /// Interaction logic for Activacion.xaml
    /// </summary>
    public partial class Activacion : Window
    {
        public Activacion()
        {
            InitializeComponent();

        }
      
        public bool HasUserExited { get; private set; }
        public Installation LoggedInstallation { get; private set; }

        private void AuthenticateInstallation()
        {
            try
            {
                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                LoggedInstallation = dataAccessLayer.AuthenticateInstallation(this.usernameTb.Text, this.passwordTb.Password, this.actkeyTb.Text, this.Ubicaciontb.Text);
                this.DialogResult = LoggedInstallation != null;
            }
            catch (Exception errorConnecting)
            {
                MessageBox.Show(errorConnecting.Message, "Revise Conexion a Internet", MessageBoxButton.OK, MessageBoxImage.Error);
                this.DialogResult = null;
            }
        }


        private void IngresarBtn_Click(object sender, RoutedEventArgs e)
        {
            AuthenticateInstallation();
        }

        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            HasUserExited = true;
            this.DialogResult = false;
        }

        private void image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.mediventa.com");
        }

     
    }
}
