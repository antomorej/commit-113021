﻿using MCMEDLink.Controls;
using MedLinkNet.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MedLinkNet.Model.Connection;

namespace MCMEDLink.Windows
{
  /// <summary>
  /// Interaction logic for Window1.xaml
  /// </summary>
  public partial class UserEdit : Window
  {
    private bool newuser;

    DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
    UserView thisUserView;

    List<WorkGroup> GrupoList = new List<WorkGroup> { };
    List<User> EnviaList = new List<User> { };

    List<string> GrupoListItems = new List<string> {};
    List<string> TipoListItems = new List<string> { };

    void CreateGrupoListItems(List<WorkGroup> grupoList)
    {
      GrupoListItems.Clear();
      foreach (WorkGroup item in grupoList) 
        {
          GrupoListItems.Add(item.ToString());
        }
    }

    void CreateTipoListItems(List<UserType> tipoList)
    {
      TipoListItems.Clear();
      foreach (UserType item in tipoList)
      {
        TipoListItems.Add(item.ToString());
      }
    }

    public UserEdit(UserView userView, bool nuevo)
    {
      InitializeComponent();

      thisUserView = userView; // guardar lo inicial
      newuser = nuevo;

      if (thisUserView != null)
      {

        GrupoList = dataAccessLayer.GetAllWorkGroupsThisTest(userView.TestID); // solo los del mismo testid
        CreateGrupoListItems(GrupoList);
        GrupoCb.ItemsSource = GrupoList;

        List<User> DrList = dataAccessLayer.GetAllDoctorsThisGroup(userView.GrupoID); // Dr del mismo grupo only
        UsersCb.ItemsSource = DrList;
        if (DrList != null) UsersCb.SelectedIndex = 0; // Seleccionar el primero

        List<UserType> TList = dataAccessLayer.GetUserTypeList();
        TipoCb.ItemsSource = TList;
        CreateTipoListItems(TList);

        Show(thisUserView);
      }
      
    }

    private void Show(UserView userView)
    { 
      
        if (!newuser) IDLb.Content = thisUserView.UserID; else IDLb.Content = "NUEVO";

        TestLb.Content = dataAccessLayer.GetTestName(thisUserView.TestID);

        UserBox.Text = thisUserView.Username;    
        PasswBox.Text = thisUserView.Password;   
        NombreBox.Text = thisUserView.Name;

        UserBox.IsReadOnly = !newuser;
        PasswBox.IsReadOnly = !newuser;

        string item = dataAccessLayer.GetGroup(thisUserView.GrupoID).ToString();
        GrupoCb.SelectedIndex = GrupoListItems.IndexOf(item);

        item = dataAccessLayer.GetUserType(thisUserView.UsertypeID).ToString(); 
        TipoCb.SelectedIndex = TipoListItems.IndexOf(item);

        UserEnableCB.IsChecked = thisUserView.Enabled;
        OnlineCb.IsChecked = thisUserView.Online;

        LasttimeBx.Text = thisUserView.Lasttime.ToString();

        ContactoBx.Text = thisUserView.Contacto;
        EmailBx.Text = thisUserView.Email;
        TelfBx.Text = thisUserView.Telefono;
        DireccionBx.Text = thisUserView.Direccion;
        NotasBx.Text = thisUserView.Notas;

        EquipoBx.Text = thisUserView.Equipo;
        NserieBx.Text = thisUserView.NSerie;
        LicenciaBx.Text = thisUserView.Licencia;
        FechaInstallBx.Text = thisUserView.FechaInstall;
        FechapolizaBx.Text = thisUserView.Fechapoliza;

      // mostrar los ENVIAA LIST USABNDO AssociatedUsersIDs

      EnviaList.Clear();
        if (!newuser)  // llenar el enviaa
        {
          foreach (int id in userView.AssociatedUsersIDs)
          {
            User member = dataAccessLayer.GetAllUserInfoForUserID(id);
            EnviaList.Add(member);
          }
        }
      EnviaLb.ItemsSource = null; // asegurar vacio
      EnviaLb.ItemsSource = EnviaList;
    }

    private void UpdatethisUserView()
    {

      thisUserView.Username = UserBox.Text;
      thisUserView.Password = PasswBox.Text;
      thisUserView.Name = NombreBox.Text;

      thisUserView.GrupoID = ((WorkGroup)GrupoCb.SelectedItem).IdWorkGroup;
      thisUserView.UsertypeID = ((UserType)TipoCb.SelectedItem).IDUSERTYPE;
      thisUserView.Enabled = (bool)UserEnableCB.IsChecked;

      // Formar el Enviaa String

      string sendto = "";

      foreach (User user in EnviaList)
      {
        if (sendto == "") sendto = user.UserID.ToString(); 
        else sendto = sendto + ","+ user.UserID.ToString();
      }
      thisUserView.Enviaa = sendto;

      // LasttimeBx.Text = thisUserView.Lasttime.ToString();

      thisUserView.Contacto = ContactoBx.Text;
      thisUserView.Email = EmailBx.Text;
      thisUserView.Telefono = TelfBx.Text;
      thisUserView.Direccion = DireccionBx.Text;
      thisUserView.Notas = NotasBx.Text;

      thisUserView.Equipo = EquipoBx.Text;
      thisUserView.NSerie = NserieBx.Text;
      thisUserView.Licencia = LicenciaBx.Text;

      try { DateTime.Parse(FechaInstallBx.Text); thisUserView.FechaInstall = FechaInstallBx.Text; } catch { thisUserView.Fechapoliza = "1/1/1900"; }
      try { DateTime.Parse(FechapolizaBx.Text); thisUserView.Fechapoliza = FechapolizaBx.Text; } catch { thisUserView.Fechapoliza = "1/1/1900"; }

    }
    private void CancelBn_Click(object sender, RoutedEventArgs e)
    {
      Show(thisUserView); // de nuevo el inicial
    }

    private void SaveBn_Click(object sender, RoutedEventArgs e)
    {
      // actualizar datos desde la forma

      UpdatethisUserView();

      if (newuser)  // Update / Insert User
      {
        try
        {
          dataAccessLayer.CreateNewUser(thisUserView);
        }
        catch (Exception ex)
        {
          string mensaje = ex.Message;
        }

      }
      else
      {
        try
        {
          dataAccessLayer.UpdateUser(thisUserView.UserID, thisUserView);
        }
        catch (Exception ex)
        {
          string mensaje = ex.Message;
        }
      }

      Close();
    }

    private void EnviaLb_LoadingRow(object sender, DataGridRowEventArgs e)
    {
      EnviaLb.Columns[1].Visibility = Visibility.Hidden;
      EnviaLb.Columns[2].Visibility = Visibility.Hidden;
      EnviaLb.Columns[4].Visibility = Visibility.Hidden;
      EnviaLb.Columns[5].Visibility = Visibility.Hidden;
      EnviaLb.Columns[6].Visibility = Visibility.Hidden;
      EnviaLb.Columns[7].Visibility = Visibility.Hidden;
      EnviaLb.Columns[8].Visibility = Visibility.Hidden;
      EnviaLb.Columns[9].Visibility = Visibility.Hidden;
      EnviaLb.Columns[10].Visibility = Visibility.Hidden;
      EnviaLb.Columns[11].Visibility = Visibility.Hidden;
      EnviaLb.Columns[12].Visibility = Visibility.Hidden;
      EnviaLb.Columns[13].Visibility = Visibility.Hidden;

    }

    private void EnviaLb_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (EnviaLb.Items.Count > 0) RemoveBn.IsEnabled = true; else RemoveBn.IsEnabled = false;
    }

    private void RemoveBn_Click(object sender, RoutedEventArgs e)
    {
      if ((EnviaLb.SelectedIndex >=0) & (EnviaLb.SelectedIndex < EnviaList.Count))
      { 
        EnviaList.RemoveAt(EnviaLb.SelectedIndex);
        EnviaLb.ItemsSource = null;
        EnviaLb.ItemsSource = EnviaList;
      }
    }

    private void AddBn_Click(object sender, RoutedEventArgs e)
    {
      EnviaList.Add((User)UsersCb.SelectedItem);
      EnviaLb.ItemsSource = null;
      EnviaLb.ItemsSource = EnviaList;
    }

  }
}
