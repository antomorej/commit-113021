﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;

using MedLinkNet.Model;
using MedLinkNet.Model.Connection;
using MedLinkNet.IOAccess;
using System.Collections;
using System.Windows.Resources;
using System.IO;

namespace MCMEDLink.Windows
{
    /// <summary>
    /// Interaction logic for ControlPanel.xaml
    /// </summary>
    public partial class ControlPanel : Window
    {

        public int IDUser = 0;
        public byte Erased = 0;

        private BackgroundWorker BW;
        private List<string> TransToDelete;

        private List<TransactionView> TransList = new List<TransactionView>();

        private MedLinkNetClient client = new MedLinkNetClient();

        bool Actived;

        public ControlPanel()
        {
            InitializeComponent();
        }

        private void Refresh()
        {
            DateTime today = DateTime.Today;
            DateTime finmes = today.AddDays(-today.Day);
            DateTime mesantes = finmes.AddMonths(-1);
            DateTime mannana = today.AddDays(1);

            DateTime from = today; // ASUME HOY
            DateTime to = mannana;

            if (comboMes.SelectedIndex == 1) // este mes
            {
                from = finmes.AddDays(1);
                to = mannana;
            }

            if (comboMes.SelectedIndex == 2) // mes pasado
            {
                from = mesantes.AddDays(1);
                to = finmes.AddDays(1);
            }

            if (comboMes.SelectedIndex == 3) //anteriores
            {
              from = today.AddMonths(-1200); // hace 10 annos
              to = mesantes.AddDays(1);
            }

            if (comboMes.SelectedIndex == 4) // TODOS
            {
              from = today.AddMonths(-1200); // hace 10 annos
              to = mannana;
            }

            int sender = 0;
            int receiver = 0;
            
            if (SendByCb.SelectedIndex >=  0) sender = ((User)SendByCb.SelectedItem).UserID;
            if (ToCb.SelectedIndex >= 0) receiver = ((User)ToCb.SelectedItem).UserID;
           
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            TransList = dataAccessLayer.GetAllTransactionsFilterBy(from, to,sender,receiver, Erased);
     
            TransGrid.ItemsSource = TransList;

            LCANT.Content = TransGrid.Items.Count;

            BackBn.IsEnabled = (comboMes.SelectedIndex == 4);      // SOLO BACKUP SOLO CON TODOS
            DeleteBn.IsEnabled = (comboMes.SelectedIndex == 3);    // SOLO CON ATERIORES A 2 MESES
         }
     
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            BBox.Text = client.MyBucketName;

            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
         
            List<WorkGroup> groups = dataAccessLayer.GetAllWorkGroupsThisDomain(0);
            int lastpos = groups.Count;
            WorkGroup  TodoG = new WorkGroup{ IdWorkGroup = 0, GroupName = "TODOS" };
            groups.Add(TodoG);

            GroupCb.ItemsSource = groups;
            GroupCb.SelectedIndex = lastpos; // CONTINUAR CON GRUPOCB CHAMNGE EVENT
       

          }

        private void RefreshBn_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void trashControl_ViewChange(object sender, EventArgs e)
        {
            Erased = trashControl.ViewErased;
            Refresh();
        }

        private void trashControl_StatusChange(object sender, EventArgs e)
        {
            foreach (TransactionView Item in TransGrid.SelectedItems)
            {
                string SelectedGuid = ((TransactionView)Item).TransactionGui;

                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                dataAccessLayer.SetEraseValue(SelectedGuid, trashControl.ChangeValue);
            }
            Refresh();
        }

        private void BWDeleteTrans(object sender, DoWorkEventArgs args)
        {
            try
            {
                int n = TransToDelete.Count;
                int i = 0;
                foreach (string SelectedGuid in TransToDelete)
                {
                    // delete in bucket first
                    client.DeleteFolderItems(SelectedGuid);
                    // them delete in the DB
                    DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                    dataAccessLayer.DeleteTransaction(SelectedGuid);

                    i++;
                    BW.ReportProgress(i*100 / n);
                }
            }
            catch 
            {
                BW.CancelAsync();              
            }
    
        }

        private void StartOP()
        {
            OPLABEL.Content = "IN PROGRESS";
            PB.Value = 0;
            TraceBox.Text = "";
            BW.RunWorkerAsync();
        }

        private void DeleteBn_Click(object sender, RoutedEventArgs e)
        {
            // Create TransToDelete List
            //
            TransToDelete=new List<string>();
            foreach(TransactionView trans in TransGrid.SelectedItems)
            {
                TransToDelete.Add(trans.TransactionGui);
            }

            // Execute delete BW Thread
            //
            BW = new BackgroundWorker();
            BW.WorkerSupportsCancellation = true;
            BW.WorkerReportsProgress = true;
            BW.ProgressChanged += BWProgressChanged;
            BW.RunWorkerCompleted += BWRunWorkerCompleted;
            BW.DoWork += BWDeleteTrans;
            StartOP();
        }

        private void BWProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PB.Value = e.ProgressPercentage;
            TraceBox.Text = client.trace;
        }

        private void BWRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled==true) OPLABEL.Content = "CANCELLED";
            else OPLABEL.Content = "OK";
            Refresh();
        }

        private void SelectAllBn_Click(object sender, RoutedEventArgs e)
        {
            TransGrid.SelectAll();
        }

        private void BWCleanAWS(object sender, DoWorkEventArgs args)
        {
            CleanAWS();
        }

        private void CleanAWS()
        {
        try
            {                
                // GET FORDER NAMES IN BUCKET
                List<string> DirNames = client.GetFolderNames();

                 // Prepare find list
                 List<string> Flist = new List<string> { };
                 foreach (TransactionView item in TransList) Flist.Add(item.TransactionGui);

                int n = DirNames.Count;
                int i = 0;
                foreach (string Dir in DirNames) // Delete the DirNames  not found in TransList
                {
                    if (! Flist.Contains(Dir) )
                     {
                      //  client.DeleteFolderItems(Dir);
                     }

                    i++;
                    BW.ReportProgress(i*100 / n);
                }
            }

            catch 
            {
                BW.CancelAsync();
            }
        }

    private void BWSaveAWS(object sender, DoWorkEventArgs args)
    {
      SaveCurrentData();
    }
   
    private void SaveCurrentData()
    {
     
        int i = 0; int n = TransList.Count;
      try
      {

        foreach (TransactionView item in TransList)
        {
          string from = item.TransactionGui;

          try
          {
            client.CopyFolder(from);
          }
          finally
          {
            i++;
            BW.ReportProgress(i * 100 / n);
          }
          
        }
      }

      catch
      {
        BW.ReportProgress(i * 100 / n);
        BW.CancelAsync();
      }
    }

    private void BackupBn_Click(object sender, RoutedEventArgs e)
    {
      client.MyBucketName = BBox.Text;

      BW = new BackgroundWorker();
      BW.WorkerSupportsCancellation = true;
      BW.WorkerReportsProgress = true;
      BW.ProgressChanged += BWProgressChanged;
      BW.RunWorkerCompleted += BWRunWorkerCompleted;
      BW.DoWork += BWSaveAWS;

      StartOP();

      //TraceBox.Text = "";
      //OPLABEL.Content = "IN PROGRESS";
     // PB.Value = 0;
     // SaveCurrentData();
      
    }

    private void CleanBn_Click(object sender, RoutedEventArgs e)
        {
            client.MyBucketName= BBox.Text;

            BW = new BackgroundWorker();
            BW.WorkerSupportsCancellation = true;
            BW.WorkerReportsProgress = true;
            BW.ProgressChanged += BWProgressChanged;
            BW.RunWorkerCompleted += BWRunWorkerCompleted;
            BW.DoWork += BWCleanAWS;
           
            StartOP();       
        }

        private void BW_DoWork(object sender, DoWorkEventArgs e)
        {
          throw new NotImplementedException();
        }

        private void BWCleanPatients(object sender, DoWorkEventArgs args)
        {
            try
            {
              DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
              dataAccessLayer.DeleteOldPatients();
            }

            catch 
            {
                BW.CancelAsync();
            }
        }

        private void CleanPatBn_Click(object sender, RoutedEventArgs e)
        {
            BW = new BackgroundWorker();
            BW.WorkerSupportsCancellation = true;
            BW.WorkerReportsProgress = true;
            BW.ProgressChanged += BWProgressChanged;
            BW.RunWorkerCompleted += BWRunWorkerCompleted;
            BW.DoWork += BWCleanPatients;
            StartOP(); 
        }

        private void comboMes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Actived) Refresh();
        }

        private void ChangeStatus_Click(object sender, RoutedEventArgs e)
        {
            foreach (TransactionView Item in TransGrid.SelectedItems)
            {
                string SelectedGuid = ((TransactionView)Item).TransactionGui;

                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                dataAccessLayer.SetTransactionStatus(SelectedGuid, comboStatus.SelectedIndex+1);
            }
            Refresh();
        }

    private void TransGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      TransactionView Item = (TransactionView)TransGrid.CurrentItem;
      if (Item != null) IDBOX.Text = Item.TransactionGui;
      else IDBOX.Text= "";

    }

    private void GroupCb_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();

      int gid = ((WorkGroup)GroupCb.SelectedItem).IdWorkGroup;
      List<User> UserList = dataAccessLayer.GetAllActiveUserList(gid);

      int lastpos = UserList.Count;
      User Todos = new User { UserID = 0, Name = "TODOS" };
      UserList.Add(Todos);

      SendByCb.ItemsSource = UserList;
      if (gid==0) SendByCb.SelectedIndex = lastpos; else SendByCb.SelectedIndex = 0;

      ToCb.ItemsSource = UserList;     
      ToCb.SelectedIndex = lastpos;

      if (!Actived)
      {
        Refresh();
        Actived = true;
      }
    }
  }
}
