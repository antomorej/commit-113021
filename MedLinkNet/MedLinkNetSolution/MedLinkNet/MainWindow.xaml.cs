﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.IO;
using MedLinkNet.Model;
using MCMEDLink.Controls;
using MedLinkNet.Model.Connection;
using MCMEDLink.Windows;

namespace MCMEDLink
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private Fields
        private static int logInTracker = 1;
        #endregion

        #region Private Properties
        
        private User CurrentUser { get; set; }
        private Installation CurrentInstallation { get; set; }
        private bool IsUserLoggedIn { get; set; }
        private bool IsInstallationLoggedIn { get; set; }
        private int SelectedGrid = 0;  // 0 None, 1 ReceivedGrid , 2 SenderGrid 
        private int fPID;

        // To active PatientData View and keep Selected Patient ID
        private int SelectedPID
        {
            get { return fPID; }
            set 
            {
                fPID = value;

                if (fPID > 0)  PatientImg.Visibility = Visibility.Visible;
                else PatientImg.Visibility = Visibility.Hidden;
               
            }
        }

        #endregion

        #region Private Objects
        private TimerClass mytimer = new TimerClass();
    #endregion

    public MainWindow()
    {
      InitializeComponent();
      SetDefaultViewProperties();

      AskForUserToLogIn();
      if (IsUserLoggedIn)
      {
        this.currentUserCu.LoadUser(CurrentUser);      
      }
      else Application.Current.Shutdown();

    }
      

        #region Helpers

        private void SetDefaultViewProperties()
        {
            this.WindowState = WindowState.Normal;
        }

        private void PopulateSendOptions(User fromUser)
        {
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            List<User> associatedUsers = null;
            enviarAcbx.SelectedItem = -1;
          if (fromUser != null)
           {
        associatedUsers = fromUser.AssociatedUsersIDs.Select(dataAccessLayer.GetAllUserInfoForUserID).ToList();
        enviarAcbx.ItemsSource = associatedUsers;
        enviarAcbx.SelectedValuePath = "UserID";
        if (associatedUsers.Count > 0) enviarAcbx.SelectedItem = associatedUsers[0];
       
           }
        }

        private void SetPaths()
        {
            this.ReadFe.Text = SelectExamPath();
            this.WriteFe.Text = SelectExamPath();
            this.ReadFeRe.Text = SelectReportPath();
            this.WriteFeRp.Text = SelectReportPath();
        }

        private string SelectExamPath()
        {
            string path = path = Properties.Settings.Default.ExamPath;
            string exam = null;
            if (CurrentUser!=null) exam = CurrentUser.Test;

            if (exam == Properties.Settings.Default.ExamID) path = Properties.Settings.Default.ExamPath;
            else
                if (exam == Properties.Settings.Default.ExamID2) path = Properties.Settings.Default.ExamPath2;
                else
                    if (exam == Properties.Settings.Default.ExamID3) path = Properties.Settings.Default.ExamPath3;

            return path;
        }

        private string SelectReportPath()
        {
            string path = Properties.Settings.Default.ReportPath;
            string exam = CurrentUser.Test;

            if (exam == Properties.Settings.Default.ExamID) path = Properties.Settings.Default.ReportPath;
            else
                if (exam == Properties.Settings.Default.ExamID2) path = Properties.Settings.Default.ReportPath2;
                else
                    if (exam == Properties.Settings.Default.ExamID3) path = Properties.Settings.Default.ReportPath3;

            return path;
        }           
        #endregion

        #region Authentication Logic
        private void AskForUserToLogIn()
        {
            IsUserLoggedIn = false; 
            Login logInScreen = new Login(); 
            bool? result = logInScreen.ShowDialog();

            CurrentUser = logInScreen.LoggedInUser;

            if (logInScreen.HasUserExited)
            {
                // Application.Current.Shutdown();
                return;
            }

            if (result != null && result.Value)
            {
                IsUserLoggedIn = true;

            if (CurrentUser.Usertype <= 1)  // Open Control Panel for Admnistrators only !!
              {
                PanelControl.IsEnabled = true;

              }
              else
              {
                PanelControl.IsEnabled = false;
                
              }

                if ((CurrentUser.TestID == 2) | (CurrentUser.TestID == 3))  // Si Es ECG object Stress
                {
                    sendTi.IsEnabled = false;            // Deshabilitar funciones
                    ReceiveExamTi.IsEnabled = false;
                    SendReportTi.IsEnabled = false;
                    ReceiveReportTi.IsEnabled = false;
                }
                else
                {
                    sendTi.IsEnabled = true;            // Habilitar todas primero
                    ReceiveExamTi.IsEnabled = true;
                    SendReportTi.IsEnabled = true;
                    ReceiveReportTi.IsEnabled = true;
                }
                
                return;
            }

            if (logInTracker < Properties.Settings.Default.LogInRetries)
            {
                MessageBox.Show("Nombre de usuario o contraseña incorrecta !!.  Por favor, intente otra vez.", "Login", MessageBoxButton.OK, MessageBoxImage.Error);
                logInTracker++;
                AskForUserToLogIn();
            }
            else
            {
                MessageBox.Show("Ha excedido la maxima cantidad de intentos permitidos !!.", "Login", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
                return;
            }
        }
        private void AskForActivation()
        {
            IsInstallationLoggedIn = false; 
            Activacion logInScreen = new Activacion();
            bool? result = logInScreen.ShowDialog();

            CurrentInstallation = logInScreen.LoggedInstallation;

            if (result != null && result.Value)
            {
                IsInstallationLoggedIn = true;
                return;
            }

            if (logInScreen.HasUserExited)
            {
                Application.Current.Shutdown();
                return;
            }

            if (logInTracker < Properties.Settings.Default.LogInRetries)
            {
                MessageBox.Show("Datos Incorectos !!. Intente de nuevo.", "Activación", MessageBoxButton.OK, MessageBoxImage.Error);
                logInTracker++;
                AskForActivation();
            }
            else
            {
                MessageBox.Show("Ha excedido la maxima cantidad de intentos permitidos !!.", "Activación", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
                return;
            }
        }

        #endregion

        #region Event Handlers

        private void ConfigureMiClick(object sender, RoutedEventArgs e)
        {
            Configure configWindows = new Configure();
            configWindows.ShowDialog();           
            SetPaths(); // Set the default paths again after config changes         
        }

        private void ReloginClick(object sender, RoutedEventArgs e)
        {
            // Put the User Offline before relogin
            CurrentUser.Online = false;

            AskForUserToLogIn();

            if (IsUserLoggedIn)
            {
                this.currentUserCu.LoadUser(CurrentUser);
                RefreshTransactions(sender, e);
            }
        }

        private void PanelClick(object sender, RoutedEventArgs e)
        {
            ControlPanel panel = new ControlPanel();
            panel.ShowDialog();
        }
        private void UserClick(object sender, RoutedEventArgs e)
        {
          ControlUser panel = new ControlUser();
          panel.ShowDialog();
        }

    #endregion

    private void TransactionTiLoaded(object sender, RoutedEventArgs e)
        {
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            byte Erased = trashControl.ViewErased;
            List<Transaction> receivedTrans = dataAccessLayer.GetAllTransactionsSentToThisUser(this.CurrentUser,Erased);
            ReceivedGrid.ItemsSource = receivedTrans;
            TotalIn.Content = ReceivedGrid.Items.Count;

            List<Transaction> senderTrans = dataAccessLayer.GetAllTransactionsSentByThisUser(this.CurrentUser,Erased);
            SenderGrid.ItemsSource = senderTrans;
            TotalOut.Content = SenderGrid.Items.Count;
            SelectedPID = 0;
        }

        private void SendTiLoaded(object sender, RoutedEventArgs e)
        {
            // Default to initial path setup in the configuration settings
            this.ReadFe.Text = SelectExamPath();
            ReadFe.Validate();
            //Populate SentTo options
            this.PopulateSendOptions(this.CurrentUser);
        }

        private void ReceiveExamTiLoaded(object sender, RoutedEventArgs e)
        {
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            byte All = 0;
            if (SeeAllNewExams.IsChecked == true)  All=1;
            List<Transaction> receivedTrans = dataAccessLayer.GetAllNewExamsSentToThisUser(this.CurrentUser, All);
            NewExamsGrid.ItemsSource = receivedTrans;
           
            // Default to initial path setup in the configuration settings
            this.WriteFe.Text = SelectExamPath();
            WriteFe.Validate();
        }

        private void SendReportTiLoaded(object sender, RoutedEventArgs e)
        {
            byte All = 0;
            if (SeeAllExamsToResponse.IsChecked == true) All = 1;
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            List<Transaction> receivedTrans= dataAccessLayer.GetAllReceivedExamsSentToThisUser(this.CurrentUser,All);
            ReceivedExamsGrid.ItemsSource = receivedTrans;

            // Default to initial path setup in the configuration settings
            this.ReadFeRe.Text = SelectReportPath();
            ReadFeRe.Validate();
        }

        private void ReceiveReportTiLoaded(object sender, RoutedEventArgs e)
        {
            byte All = 0;
            if (SeeAllReportToReceive.IsChecked == true) All = 1;
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            List<Transaction> receivedTrans = dataAccessLayer.GetAllNewReportsSentToThisUser(this.CurrentUser,All);
            ReportsToReceiveGrid.ItemsSource = receivedTrans;
            // Default to initial path setup in the configuration settings
            this.WriteFeRp.Text = SelectReportPath();
            WriteFeRp.Validate();
        }

        private void EnviarBtnClick(object sender, RoutedEventArgs e)
        {
           
            if (ReadFe.ValidEntry)  EnviarLb.Content = "";
            else
            {
                EnviarLb.Content = "Verifique Ruta del Examen ..";
                return;
            } 
                
            string transactionGui = Guid.NewGuid().ToString();
            string fromFolder = this.ReadFe.Text;

            string destinationFolder = System.IO.Path.Combine(Properties.Settings.Default.TransPath, transactionGui);
            // For AWS Destination Path is only folder name 
            if (Properties.Settings.Default.AWSEnabled == true) destinationFolder = transactionGui;

            CopyWindow copyWindow = new CopyWindow();
            this.IsEnabled = false;
            bool sucess = copyWindow.StartCopy(fromFolder, destinationFolder, ReadFe.FilePathType, true);
            copyWindow.Close();
            this.IsEnabled = true;
            
            if (sucess==true)
            {
                // Record transaction on the Transaction table.
                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                dataAccessLayer.RecordTransaction(this.CurrentUser, (User)enviarAcbx.SelectedItem, transactionGui, 1, patientCodeTb.Text, patientNameTb.Text, edadTb.Text, sexoTb.Text, obsTb.Text);
                RefreshTransactions(sender, e);
            }
        }

        private void RecibirBtnClick(object sender, RoutedEventArgs e)
        {
            if (WriteFe.ValidEntry) EnviarLb.Content = "";
            else
            {
                RecibirLb.Content = "Verifique Ruta del Examen ..";
                return;
            } 
           
            string transactionGui = ((Transaction)NewExamsGrid.SelectedItem).TransactionGui;
            string fromFolder = System.IO.Path.Combine(Properties.Settings.Default.TransPath, transactionGui);

            // For AWS FromFolder Path is only folder name
            if (Properties.Settings.Default.AWSEnabled == true) fromFolder = transactionGui;

            string destinationFolder = this.WriteFe.Text;

            CopyWindow copyWindow = new CopyWindow();
            this.IsEnabled = false;
            bool sucess = copyWindow.StartCopy(fromFolder, destinationFolder,false,false);
            copyWindow.Close();
            this.IsEnabled = true;

            if (sucess == true)
            {
                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                dataAccessLayer.SetTransactionStatus(transactionGui, 2);
                RefreshTransactions(sender, e);
            }
        }
        
        private void ResponderBtnClick(object sender, RoutedEventArgs e)
        {
            if (ReadFeRe.ValidEntry) EnviarLb.Content = "";
            else
            {
                ResponderLb.Content = "Verifique Ruta del Reporte ..";
                return;
            } 

            string transactionGui = Guid.NewGuid().ToString();
           

            Transaction selectedTransaction = ((Transaction) ReceivedExamsGrid.SelectedItem);
            string GuiExam = selectedTransaction.TransactionGui;
            Int32 ToID = selectedTransaction.RemitenteID;
            
            User sendToUser = new User { UserID = ToID  }; // ENVIAR AL MISMO QUE LO ENVIO

            string fromFolder = this.ReadFeRe.Text;
            string destinationFolder = System.IO.Path.Combine(Properties.Settings.Default.TransPath, transactionGui);

            // For AWS Destination Path is only folder name
            if (Properties.Settings.Default.AWSEnabled == true) destinationFolder = transactionGui;

            CopyWindow copyWindow = new CopyWindow();
            this.IsEnabled = false;
            bool sucess = copyWindow.StartCopy(fromFolder, destinationFolder,ReadFeRe.FilePathType,true);
            copyWindow.Close();
            this.IsEnabled = true;

            if (sucess == true)
            {
                // Record transaction on the Transaction table.
                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                dataAccessLayer.SetTransactionStatus(GuiExam, 3); // indicar constestado
                // nueva transaction as informe enviado

                dataAccessLayer.RecordTransaction(this.CurrentUser, sendToUser,  transactionGui, 2, selectedTransaction.PacienteID );
                // refrescar
                RefreshTransactions(sender, e);
            }
        }

        private void ReceiveReportBtnClick(object sender, RoutedEventArgs e)
        {

            if (WriteFeRp.ValidEntry) EnviarLb.Content = "";
            else
            {
                ReceiveReportLb.Content = "Verifique Ruta del Reporte ..";
                return;
            } 

            string transactionGui = ((Transaction)ReportsToReceiveGrid.SelectedItem).TransactionGui;
            string fromFolder = System.IO.Path.Combine(Properties.Settings.Default.TransPath, transactionGui);
            // For AWS FromFolder Path is only folder name
            if (Properties.Settings.Default.AWSEnabled == true) fromFolder = transactionGui;

            string destinationFolder = this.WriteFeRp.Text;

            CopyWindow copyWindow = new CopyWindow();
            this.IsEnabled = false;
            bool sucess = copyWindow.StartCopy(fromFolder, destinationFolder,false,false);
            copyWindow.Close();
            this.IsEnabled = true;
            
            if (sucess==true)
            {
                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                dataAccessLayer.SetTransactionStatus(transactionGui, 2);
                RefreshTransactions(sender, e);
            }

        }

        private void EnviarDataChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            // Valida los datos para enviar examen
            if ((patientCodeTb.Text.Length > 0) & (patientNameTb.Text.Length > 0))
            {
                EnviarLb.Content = "";
                EnviarBtn.IsEnabled = true;     
            }
            else
            {
                EnviarBtn.IsEnabled = false;
                EnviarLb.Content = "Complete los datos ..";
            }

        }

        private void NewExamsGridSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // Valida los datos para recibir examen
            if (NewExamsGrid.SelectedItems.Count>0)
            {
                RecibirBtn.IsEnabled = true;
                RecibirLb.Content = "";

                Transaction selectedTransaction = ((Transaction)NewExamsGrid.SelectedItem);
                SelectedPID = selectedTransaction.PacienteID;
            }
            else
            {
                RecibirBtn.IsEnabled = false;
                RecibirLb.Content = "Seleccione Paciente";
                SelectedPID = 0;
            }

        }

        private void ReceivedExamsGridSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // Valida los datos para enviar reporte
            if (ReceivedExamsGrid.SelectedItems.Count > 0)
            {
                ResponderBtn.IsEnabled = true;
                ResponderLb.Content = "";
                Transaction selectedTransaction = ((Transaction)ReceivedExamsGrid.SelectedItem);
                SelectedPID = selectedTransaction.PacienteID;
            }
            else
            {
                ResponderBtn.IsEnabled = false;
                ResponderLb.Content = "Seleccione Paciente";
                SelectedPID = 0;
            }

        }

        private void ReportsToReceiveGridSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // Valida los datos para recibir reporte
           
               if (ReportsToReceiveGrid.SelectedItems.Count > 0)
               {
                   ReceiveReportBtn.IsEnabled = true;
                   ReceiveReportLb.Content = "";
                   Transaction selectedTransaction = ((Transaction)ReportsToReceiveGrid.SelectedItem);
                   SelectedPID = selectedTransaction.PacienteID;
               }
               else
               {
                   ReceiveReportBtn.IsEnabled = false;
                   ReceiveReportLb.Content = "Seleccione Paciente";
                   SelectedPID = 0;
               } 
        }

        private void NewExamsGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
           NewExamsGrid.Columns[0].Visibility = Visibility.Hidden;
           NewExamsGrid.Columns[2].Visibility = Visibility.Hidden;
           NewExamsGrid.Columns[7].Visibility = Visibility.Hidden;
           NewExamsGrid.Columns[8].Visibility = Visibility.Hidden;
           NewExamsGrid.Columns[9].Visibility = Visibility.Hidden;
        }

        private void ReceivedExamsGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
            ReceivedExamsGrid.Columns[0].Visibility = Visibility.Hidden;
            ReceivedExamsGrid.Columns[2].Visibility = Visibility.Hidden;
            ReceivedExamsGrid.Columns[7].Visibility = Visibility.Hidden;
            ReceivedExamsGrid.Columns[8].Visibility = Visibility.Hidden;
            ReceivedExamsGrid.Columns[9].Visibility = Visibility.Hidden;
        }

        private void ReportsToReceiveGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {   
            ReportsToReceiveGrid.Columns[0].Visibility = Visibility.Hidden;
            ReportsToReceiveGrid.Columns[2].Visibility = Visibility.Hidden;
            ReportsToReceiveGrid.Columns[7].Visibility = Visibility.Hidden;
            ReportsToReceiveGrid.Columns[8].Visibility = Visibility.Hidden;
            ReportsToReceiveGrid.Columns[9].Visibility = Visibility.Hidden;
        }

        private void ReceivedGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
           ReceivedGrid.Columns[0].Visibility = Visibility.Hidden;
           ReceivedGrid.Columns[2].Visibility = Visibility.Hidden;
           ReceivedGrid.Columns[7].Visibility = Visibility.Hidden;
           ReceivedGrid.Columns[8].Visibility = Visibility.Hidden;
           ReceivedGrid.Columns[9].Visibility = Visibility.Hidden;
        }

        private void SenderGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
          SenderGrid.Columns[0].Visibility = Visibility.Hidden;
          SenderGrid.Columns[2].Visibility = Visibility.Hidden;
          SenderGrid.Columns[6].Visibility = Visibility.Hidden;
          SenderGrid.Columns[7].Visibility = Visibility.Hidden;
          SenderGrid.Columns[9].Visibility = Visibility.Hidden;
        }

        private void topImage_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.medlinknet.com");
        }

        private void trashControl_ViewChange(object sender, EventArgs e)
        {
            RoutedEventArgs ea = new RoutedEventArgs();
            TransactionTiLoaded(sender, ea);
        }

        private void trashControl_StatusChange(object sender, EventArgs e)
        {
            if (SelectedGrid == 1)
            {
                foreach (Transaction Item in ReceivedGrid.SelectedItems)
                {
                    string SelectedGuid = ((Transaction)Item).TransactionGui;
                    DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                    dataAccessLayer.SetEraseValue(SelectedGuid, trashControl.ChangeValue);
                }
            }

            if (SelectedGrid == 2)
            {
                foreach (Transaction Item in SenderGrid.SelectedItems)
                {
                    string SelectedGuid = ((Transaction)Item).TransactionGui;
                    DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                    dataAccessLayer.SetEraseValue(SelectedGuid, trashControl.ChangeValue);
                }
            }
        }

        private void ReceivedGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (ReceivedGrid.SelectedItems.Count > 0)
            {
                SelectedGrid = 1;
                SenderGrid.SelectedIndex = -1;

                Transaction selectedTransaction = ((Transaction)ReceivedGrid.SelectedItem);
                SelectedPID = selectedTransaction.PacienteID;
            }
            else
            {
                SelectedGrid = 0;
                SelectedPID = 0;
            }
        }

        private void SenderGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (SenderGrid.SelectedItems.Count > 0)
            {
                SelectedGrid = 2;
                ReceivedGrid.SelectedIndex = -1;

                Transaction selectedTransaction = ((Transaction)SenderGrid.SelectedItem);
                SelectedPID = selectedTransaction.PacienteID;
            }
            else
            {
                SelectedGrid = 0;
                SelectedPID = 0;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SelectedPID = 0;
        }

        private void CheckUnits(object sender, EventArgs e)
        {
            if (mainTab.SelectedIndex == 1) ReadFe.Validate(); // To detect USB unit inserted for any operation
            if (mainTab.SelectedIndex == 2) WriteFe.Validate();
            if (mainTab.SelectedIndex == 3) ReadFeRe.Validate();
            if (mainTab.SelectedIndex == 4) WriteFeRp.Validate();
        }

        private void RefreshTransactions(object sender, RoutedEventArgs e)
        {
            TransactionTiLoaded(sender, e);
            SendTiLoaded(sender, e);
            ReceiveExamTiLoaded(sender, e);
            SendReportTiLoaded(sender, e);
            ReceiveReportTiLoaded(sender, e);
        }

        private void PatientImg_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            PatientView pview = new PatientView(SelectedPID);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Put the User Offline 
            if (CurrentUser!=null)
                CurrentUser.Online = false;
        }

 
    }
}
