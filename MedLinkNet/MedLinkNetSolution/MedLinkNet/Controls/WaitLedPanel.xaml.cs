﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace MCMEDLink.Controls
{
    /// <summary>
    /// Interaction logic for WaitPanel.xaml
    /// </summary>
    public partial class WaitLedPanel : UserControl
    {
        private TimerClass mytimer = new TimerClass();
        private SolidColorBrush red = new SolidColorBrush(Colors.Red);
        private SolidColorBrush gray = new SolidColorBrush(Colors.Gray);
        private SolidColorBrush green = new SolidColorBrush(Colors.LawnGreen);
        private SolidColorBrush yellow = new SolidColorBrush(Colors.Yellow);
        public bool flashing;
        private int status;

        public WaitLedPanel()
        {
            InitializeComponent();
            status = 1;
            flashing = false;
        }

        public void State(int state, int wait)
        {
            switch (state)
            {
                case 1:
                    if (flashing) led1.Fill = green; else led1.Fill  = red;
                    led2.Fill = gray;
                    led3.Fill = gray;
                    break;
                case 2:
                    led1.Fill  = gray;
                    if (flashing) led2.Fill = green; else led2.Fill = yellow;
                    led3.Fill = gray;
                    break;
                case 3:
                    led1.Fill = gray;
                    led2.Fill = gray;
                    led3.Fill = green;
                    break;
            }

          System.Windows.Forms.Application.DoEvents();
          System.Threading.Thread.Sleep(wait);
        }

        public void Start()
        {
            mytimer.StarTimer(1000, eventcall);
            flashing = true;
        }

        public void Flash()
        {
            flashing = true;
            if (status < 3)
                status++;
            else
                status = 1;
            State(status, 0);
        }

        public void eventcall(object sender, EventArgs e)
        {
            Flash();
        }

        public void Stop()
        {
            mytimer.StopTimer();
            flashing = false;
        }

        private void WaitPanel_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
