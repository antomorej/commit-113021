﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Media;
using UserControl = System.Windows.Controls.UserControl;

namespace MCMEDLink.Controls
{
    /// <summary>
    /// Interaction logic for FolderEntry.xaml
    /// </summary>
    public partial class FolderEntry : UserControl
    {
        public bool ValidEntry = false;   // indica si existe la ruta escogida
        public bool FilePathType = false; // Indica si la ruta es un file sino es un folder
       
        public static DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(FolderEntry), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty DescriptionProperty = DependencyProperty.Register("Description", typeof(string), typeof(FolderEntry), new PropertyMetadata(null));

        public string Text { get { return GetValue(TextProperty) as string; } set { SetValue(TextProperty, value); } }

        public string Description { get { return GetValue(DescriptionProperty) as string; } set { SetValue(DescriptionProperty, value); } }

        public FolderEntry() { InitializeComponent(); }

        private void FolderOp_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
           
            {
                dlg.Description = Description;
                dlg.SelectedPath = Text;
                dlg.ShowNewFolderButton = true;
                DialogResult result = dlg.ShowDialog();
                if (result == DialogResult.OK)
                {
                    FilePathType = false; 
                    Text = dlg.SelectedPath;
                    BindingExpression be = GetBindingExpression(TextProperty);
                    if (be != null)
                        be.UpdateSource();
                }
            }

            Validate();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           Validate();
        }

        public void Validate()
        {
            SolidColorBrush red = new SolidColorBrush(Colors.Red);
            SolidColorBrush green = new SolidColorBrush(Colors.LawnGreen);

            if (FilePathType == true) ValidEntry = (File.Exists(EntryBx.Text));
            else ValidEntry = (Directory.Exists(EntryBx.Text));

            if (ValidEntry == true)
            {
                Led.Fill = green;
            }
            else Led.Fill = red;
        }


        private void fileOp_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            // dlg.DefaultExt = ".txt";
            // dlg.Filter = "Text documents (.txt)|*.txt";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file 
            if (result == true)
            {
                FilePathType = true;
                Text = dlg.FileName;
                BindingExpression be = GetBindingExpression(TextProperty);
                if (be != null)
                    be.UpdateSource();
            }

            Validate();

        }

        private void DockPanel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Validate();
        }

    }
}

