﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.IO;


namespace MCMEDLink.Controls
{
  public static class Utils
  {
    public static void SaveInstallationLocalData()
    {
      List<string> localData = new List<string> { };

      // SAVE APPLICATION PROPERTIES in local data

      localData.Add(Properties.Settings.Default.UserName);
      localData.Add(Properties.Settings.Default.Passw);

      localData.Add(Properties.Settings.Default.ExamID);
      localData.Add(Properties.Settings.Default.ExamID2);
      localData.Add(Properties.Settings.Default.ExamID3);

      localData.Add(Properties.Settings.Default.ExamPath);
      localData.Add(Properties.Settings.Default.ExamPath2);
      localData.Add(Properties.Settings.Default.ExamPath3);

      localData.Add(Properties.Settings.Default.ReportPath);
      localData.Add(Properties.Settings.Default.ReportPath2);
      localData.Add(Properties.Settings.Default.ReportPath3);


      string userFilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
      string userFilesGoHere = Path.Combine(userFilePath, "/pcopy");

      if (!Directory.Exists(userFilesGoHere)) Directory.CreateDirectory(userFilesGoHere);

      string FilePath = Path.Combine(userFilesGoHere, "Local2.dat");

      ObjectToFile(localData, FilePath);     // Convert to file and save         
    }

    public static void ReadInstallationLocalProperties()
    {
      string userFilePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
      string userFilesGoHere = Path.Combine(userFilePath, "/pcopy");

      string FilePath = Path.Combine(userFilesGoHere, "Local2.dat");

      if (File.Exists(FilePath))
      {
        try
        {
          Stream stream = File.Open(FilePath, FileMode.Open);
          System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bformatter
              = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

          List<string> localData = (List<string>)bformatter.Deserialize(stream); /// get from local file
          stream.Close();

          // GET PROPERTIES from local data

          Properties.Settings.Default.UserName = localData[0];
          Properties.Settings.Default.Passw = localData[1];
          Properties.Settings.Default.ExamID = localData[2];
          Properties.Settings.Default.ExamID2 = localData[3];
          Properties.Settings.Default.ExamID3 = localData[4];

          Properties.Settings.Default.ExamPath = localData[5];
          Properties.Settings.Default.ExamPath2 = localData[6];
          Properties.Settings.Default.ExamPath3 = localData[7];

          Properties.Settings.Default.ReportPath = localData[8];
          Properties.Settings.Default.ReportPath2 = localData[9];
          Properties.Settings.Default.ReportPath3 = localData[10];

          Properties.Settings.Default.Activated = true;
          Properties.Settings.Default.Save();

        }
        catch
        {
          // Cant not be read. possible corrupted // assume not installed
          Properties.Settings.Default.Activated = false; ;
          Properties.Settings.Default.Save();
        }
      }
      else
      {
        Properties.Settings.Default.Activated = false; ;
        Properties.Settings.Default.Save();

      }

    }

    public static bool ObjectToFile(object _Object, string _FileName)
    {
      try
      {
        // create new memory stream
        System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream();

        // create new BinaryFormatter
        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                    = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        // Serializes an object, or graph of connected objects, to the given stream.
        _BinaryFormatter.Serialize(_MemoryStream, _Object);

        // convert stream to byte array
        byte[] _ByteArray = _MemoryStream.ToArray();

        // Open file for writing
        FileStream _FileStream = new FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);

        // Writes a block of bytes to this stream using data from a byte array.
        _FileStream.Write(_ByteArray.ToArray(), 0, _ByteArray.Length);

        // close file stream
        _FileStream.Close();

        // cleanup
        _MemoryStream.Close();
        _MemoryStream.Dispose();
        _MemoryStream = null;
        _ByteArray = null;

        return true;
      }
      catch (Exception _Exception)
      {
        // Error
        MessageBox.Show(_Exception.ToString());
        return false;
      }

    }

  }
}
