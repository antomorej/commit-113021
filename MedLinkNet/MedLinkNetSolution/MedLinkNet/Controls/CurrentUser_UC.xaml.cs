﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MedLinkNet.Model;

namespace MCMEDLink.Controls
{
    /// <summary>
    /// Interaction logic for CurrentUser_UC.xaml
    /// </summary>
    public partial class CurrentUser_UC : UserControl
    {
        public CurrentUser_UC()
        {
            InitializeComponent();
        }

        public void LoadUser(User theUser)
        {
            try
            {
                this.maingb.Header = theUser.Domain;
                this.userTxb.Text = theUser.Name;
                this.workgroupTxb.Text = theUser.Grupo;
                this.testTxb.Text = theUser.Test;
                //
                // SET THE PARAMETERS LOADED FROM DATABASE FOR THIS WORKGROUP
                //
                Properties.Settings.Default.TransPath = theUser.DiskName;
                Properties.Settings.Default.JDEnabled = theUser.JDEnabled;
                Properties.Settings.Default.AWSEnabled = theUser.AWSEnabled;
               
            }
            catch (Exception)
            {
                this.workgroupTxb.Text = "";
                this.userTxb.Text = "";
            }
        }
    }
}
