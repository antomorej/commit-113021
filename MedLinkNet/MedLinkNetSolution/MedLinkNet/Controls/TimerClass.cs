﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace MCMEDLink.Controls
{
    class TimerClass
    {
        private DispatcherTimer dispatcherTimer= new DispatcherTimer();
        private EventHandler methodtocall;
        private bool running = false;

       public void StarTimer(long ticks,EventHandler method)
       {
           if (running == true) StopTimer();
           methodtocall = method;
           dispatcherTimer.Tick += new EventHandler(dispatcherTimer_call);
           dispatcherTimer.Interval = new TimeSpan(ticks); 
           dispatcherTimer.Start();
           running = true;
       }

       public void StopTimer()
       {
           dispatcherTimer.Stop();
           running = false;
       }

        private void dispatcherTimer_call(object sender, EventArgs e)
        {
            if (methodtocall != null) methodtocall(this,e);
        }
    
    }
}
