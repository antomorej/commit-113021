﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MCMEDLink.Controls
{
    /// <summary>
    /// Interaction logic for TrashControl.xaml
    /// </summary>
    public partial class TrashControl : UserControl
    {
        public TrashControl()
        {
            InitializeComponent();
        }

        public byte ViewErased = 0;    // 0 View No Erased / 1 View Erased
        public byte ChangeValue = 1;   // 1 to delete / 0 to undelete
        
        public event EventHandler ViewChange;
        public event EventHandler StatusChange;	
 
        public void RaiseChangeViewEvent()
        {
            if (ViewChange != null) ViewChange(this, new RoutedEventArgs());
        }

        public void RaiseStatusChangeEvent()
        {
            if (StatusChange != null) StatusChange(this, new RoutedEventArgs());
            RaiseChangeViewEvent();
        }

        private void TrashClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TrashClose.Visibility =Visibility.Hidden;
            TrashOpen.Visibility =Visibility.Visible;
            DeleteImg.Visibility = Visibility.Hidden;
            RestoreImg.Visibility = Visibility.Visible;
            ViewErased = 1;
            ChangeValue = 0;
            RaiseChangeViewEvent();
        }

        private void TrashOpen_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TrashOpen.Visibility = Visibility.Hidden;
            TrashClose.Visibility = Visibility.Visible;
            RestoreImg.Visibility = Visibility.Hidden;
            DeleteImg.Visibility = Visibility.Visible;
            ViewErased = 0;
            ChangeValue = 1;
            RaiseChangeViewEvent();
        }

        private void DeleteImg_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RaiseStatusChangeEvent();
            RaiseChangeViewEvent();
        }

        private void RestoreImg_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RaiseStatusChangeEvent();
            RaiseChangeViewEvent();
        }

       
    }
}