﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
    public class Transaction
    {
        public string TransactionGui { get; set; }
        public DateTime Fecha { get; set; }
        public int PacienteID { get; set; }
        public string Paciente { get; set; }
        public string Status { get; set; }
        public string Transaccion { get; set; }
        public string Remitente { get; set; }
        public int RemitenteID { get; set; }
        public string Destinatario { get; set; }
        public int DestinatarioID { get; set; }
  
    }
}
