﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
    public class UserType
    {
        public int  IDUSERTYPE
        {
            get;
            set;
        }


        public string USERTYPE
        {
          get;
          set;
        }


        public override string ToString()
            {
              return IDUSERTYPE  + " - " + USERTYPE;
            }
        }
}
