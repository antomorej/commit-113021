﻿using System.Collections.Generic;
using MedLinkNet.Model.Connection;

namespace MedLinkNet.Model
{
    public class User
    {
        public int UserID
        {
            get;
            set;
        }

        public int WorkgroupID
        {
            get;
            set;
        }

        public string Username
        {
            get;
            set;
        }

        public string Name
        {
            get; set;
        }

        public List<int> AssociatedUsersIDs
        {
            get; 
            set;
        }

        public string Domain
        {
            get;
            set;
        }

        public string Grupo
        {
            get;
            set;
        }

        public string Test
        {
            get;
            set;
        }

        public bool JDEnabled
        {
            get;
            set;
        }

        public string DiskName
        {
            get;
            set;
        }

        public bool AWSEnabled
        {
            get;
            set;
        }

        public int Usertype
        {
            get;
            set;
        }

        public bool Online
        {
            get
            {
                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                return dataAccessLayer.IsUserMarkedAsOnline(this.UserID);  
            }
            set
            {
                DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
                dataAccessLayer.ChangeUserOnlineStatus(this.UserID, value);           
            }
        }

        public int TestID { get; set; }

        public override string ToString()
        {
            return UserID + " - " + Name;
        }
    }
}
