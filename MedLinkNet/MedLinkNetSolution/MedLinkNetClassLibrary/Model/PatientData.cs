﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
    public class PatientData
    {
        public int PID { get; set; }
        public string PCode { get; set; }
        public string PName { get; set; }
        public string Edad { get; set; }
        public string Sexo { get; set; }
        public string Obs { get; set; }
    }
}
