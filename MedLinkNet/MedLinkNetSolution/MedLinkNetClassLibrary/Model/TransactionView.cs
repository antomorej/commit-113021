﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
   public class TransactionView
    {
        public int IDTrans { get; set; }
        public string TransactionGui { get; set; }
        public DateTime Fecha { get; set; }
        public string Paciente { get; set; }
        public string Remitente { get; set; }
        public string Destinatario { get; set; }
        public string TrasType { get; set; }
        public string Status { get; set; }
        public string TestName { get; set; }
    }
}
