﻿using System.Data;
using System.Data.SqlClient;
using System;
using System.Security.Policy;
using System.Net;

namespace MedLinkNet.Model.Connection
{
  public class DbConnection
  {
    private static string connectionString;                 // WIll be getting from S3
    private static SqlConnection singletonDbConnection;     // Only 1 DbConnection per Application

      public DbConnection()
      {
        using (WebClient client = new WebClient())
        {
        try
          {
          string value = client.DownloadString("https://medlinknet.s3.amazonaws.com/access/dbstring.txt");
          connectionString = value;
          }
        catch
          {
            connectionString = "Data Source=medlinknet-express.cobjtivuk6w0.us-east-1.rds.amazonaws.com;Initial Catalog=medlinknet;Persist Security Info=True;User ID=medlinknet;Password=Mianalay1";
          }
        }
      }

        /// <summary>
        /// Gets a SQL Connection. New if never created, otherwise existing open connection.
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetConnection()
        {
            if (singletonDbConnection == null || singletonDbConnection.State == ConnectionState.Closed)
            {
                try
                {
                    BuildSqlConnection();
                    return singletonDbConnection;
                }
                catch
                {
                    return null;
                }
            }
            else return singletonDbConnection;
        }

        public void CloseConnection()
        {
            if ((singletonDbConnection != null) & (singletonDbConnection.State == ConnectionState.Open))
            {
                singletonDbConnection.Close();   
            }    
        }

        /// <summary>
        /// Gets a always fresh SQL Connection, and re-initializes the current connection.
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetNewConnection(string newconnectionString)
        {
            connectionString = newconnectionString;
            BuildSqlConnection();
            return singletonDbConnection;
        }

        #region Helpers
        private void BuildSqlConnection()
        {
           singletonDbConnection = new SqlConnection(connectionString);
           singletonDbConnection.Open();    
        }

        #endregion
    }
}
