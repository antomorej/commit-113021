﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.IO;


namespace MedLinkNet.Model.Connection
{
    public class DataAccessLayer
    {
     #region Private Fields
        private static DataAccessLayer dataAccessLayer;
        public static DataAccessLayer GetDataAccessLayer()
        {
            return dataAccessLayer ?? (dataAccessLayer = new DataAccessLayer());
        }

        public bool error
        {
           get;
           set;
        }

        public string Emensaje
        {
          get;
          set;
        }

        private readonly DbConnection dbAccess = new DbConnection();

        #endregion

     #region Authentication SQLs
        public User AuthenticateUser(Int32 workgroup, string username, string password)
        {
            User newUser = null;

            try
            {

                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("SELECT USERID, NOMBRE, GROUPNAME, TESTNAME, DOMINIONAME, ENVIAA, JDENABLED, DISKNAME, AWSENABLED, IDUSERTYPE, TESTID FROM UserView WHERE USERNAME = @Username AND PASSWORD = @Password AND IDWORKGROUP = @Grupo AND ENABLED = 1", connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@Password", password);
                        command.Parameters.AddWithValue("@Grupo", workgroup);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                newUser = new User { UserID = reader.GetInt32(0), Username = username, Name = reader.GetString(1), Grupo = reader.GetString(2), Test = reader.GetString(3), Domain = reader.GetString(4), AssociatedUsersIDs = new List<int>(), JDEnabled = reader.GetBoolean(6), DiskName = reader.GetString(7), AWSEnabled = reader.GetBoolean(8), Usertype = reader.GetInt32(9), TestID = reader.GetInt32(10) };

                                string[] associatedStringIDs = new string[] { };
                                string allCommadelUsers;

                                try // Users to send to Set
                                {
                                    allCommadelUsers = reader.GetString(5);
                                }
                                catch
                                {
                                    allCommadelUsers = "";
                                }

                                if (!string.IsNullOrEmpty(allCommadelUsers))
                                {
                                    associatedStringIDs = allCommadelUsers.Split(new[] { "," }, StringSplitOptions.None);
                                }

                                foreach (var associatedStringID in associatedStringIDs)
                                {
                                    newUser.AssociatedUsersIDs.Add(Int32.Parse(associatedStringID));
                                }

                            }
                        }
                    }
                }

                newUser.Online = true; // INDICATE USER ONLINE BEFORE RETURN NEW USER
            }
            catch 
            {
                error = true;
            }
           
            return newUser;
        }

        public Installation AuthenticateInstallation(string username, string password, string actkey, string ubicacion)
        {
            Installation newInstallation = null;
            string machinename = System.Environment.MachineName; // get the Machine Name

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("SELECT IDINSTALL,SERIAL,IDUSERTYPE,TESTID,CODEID FROM INSTALLATIONS WHERE USERNAME = @Username AND PASSW = @Password AND ACTKEY = @Actkey AND INSTALLED=0", connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@Password", password);
                        command.Parameters.AddWithValue("@Actkey", actkey);
                       
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                newInstallation = new Installation
                                {
                                    IDInstall = reader.GetInt32(0),
                                    UserName = username,
                                    Passw = password,
                                    ActKey = actkey,
                                    Ubicacion = ubicacion,
                                    Installed = true,
                                    InstallDate = System.DateTime.Now,
                                    MachineName = machinename,
                                    DeviceSerial = reader.GetString(1),
                                    UserType = reader.GetInt32(2),
                                    TestID = reader.GetInt32(3),
                                    CodeID = reader.GetInt32(4)
                                };

                            }
                        }
                    }
                }
            }

            catch (Exception EX)
            {
                error = true;
                throw EX;
            }

            return newInstallation;
        }

        public Installation AuthenticateInstallation(string username, string password, string actkey, string ubicacion, int MyTestID)
        {
            Installation newInstallation = null;
            string machinename = System.Environment.MachineName; // get the Machine Name

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("SELECT IDINSTALL,SERIAL,IDUSERTYPE,TESTID,CODEID FROM INSTALLATIONS WHERE USERNAME = @Username AND PASSW = @Password AND ACTKEY = @Actkey AND TESTID = @TID AND INSTALLED=0", connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@Password", password);
                        command.Parameters.AddWithValue("@Actkey", actkey);
                        command.Parameters.AddWithValue("@TID", MyTestID);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                newInstallation = new Installation
                                {
                                    IDInstall = reader.GetInt32(0),
                                    UserName = username,
                                    Passw = password,
                                    ActKey = actkey,
                                    Ubicacion = ubicacion,
                                    Installed = true,
                                    InstallDate = System.DateTime.Now,
                                    MachineName = machinename,
                                    DeviceSerial = reader.GetString(1),
                                    UserType = reader.GetInt32(2),
                                    TestID = reader.GetInt32(3),
                                    CodeID = reader.GetInt32(4)
                                };

                            }
                        }
                    }
                }
            }

            catch (Exception EX)
            {
                error = true;
                throw EX;
            }

            return newInstallation;
        }

    #endregion

     #region Test Queries
        public List<Test> GetAllTestNames()
        {
            List<Test> TestNames = new List<Test> { };

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string selectStatement = "SELECT TESTID,TESTNAME FROM TEST";
                    using (SqlCommand command = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TestNames.Add(new Test { TESTID = reader.GetInt32(0), TESTNAME = reader.GetString(1) } );
                            }
                        }
                    }
                }
                return TestNames;
            }
            catch
            {
                error = true;
                return null;
            }
        }

        public List<string> GetAllTestThisUser(string username, string password)
        {
            List<string> TestNames = new List<string> { };

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string selectStatement = "SELECT TESTNAME FROM USERVIEW WHERE USERNAME = @Username AND PASSWORD = @Password AND ENABLED = 1";
                    using (SqlCommand command = new SqlCommand(selectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@Password", password);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TestNames.Add(reader.GetString(0));
                            }
                        }
                    }
                }
                return TestNames;
            }
            catch
            {
                error = true;
                return null;
            }
        }

    #endregion

    #region WorkGroup Queries
        public List<WorkGroup> GetAllWorkGroupsThisDomain(int dominioID)
          {
            List<WorkGroup> grupoList = new List<WorkGroup>();
            error = false;
            Emensaje = "";

            try
            {
              using (SqlConnection connection = dbAccess.GetConnection())
              {
                const string selectStatement = "SELECT IDWORKGROUP,DESCRIPTION, IDDOMINIO, IDTEST FROM WORKGROUP WHERE (IDDOMINIO = @DID OR @DID=0) AND IDWORKGROUP > 0";
                using (SqlCommand command = new SqlCommand(selectStatement, connection))
                {
                  command.Parameters.AddWithValue("@DID", dominioID);
                 
                  using (SqlDataReader reader = command.ExecuteReader())
                  {
                    while (reader.Read())
                    {
                      grupoList.Add(new WorkGroup { IdWorkGroup = reader.GetInt32(0), GroupName = reader.GetString(1), IdDominio = reader.GetInt32(2), IdTest = reader.GetInt32(3) });
                    }
                  }
                }
              }
            }
            catch (Exception eX)
            { error = true;
              Emensaje = eX.Message;
            }

            return grupoList;
          }

        public List<WorkGroup> GetAllWorkGroupsThisTest(int testID)
        {
          List<WorkGroup> grupoList = new List<WorkGroup>();
          error = false;
          Emensaje = "";

          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              const string selectStatement = "SELECT IDWORKGROUP,DESCRIPTION FROM WORKGROUP WHERE (IDTEST= @TID OR @TID=0) AND IDWORKGROUP > 0";
              using (SqlCommand command = new SqlCommand(selectStatement, connection))
              {
                command.Parameters.AddWithValue("@TID", testID);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    grupoList.Add(new WorkGroup { IdWorkGroup = reader.GetInt32(0), GroupName = reader.GetString(1) });
                  }
                }
              }
            }
          }
          catch (Exception eX)
          {
            error = true;
            Emensaje = eX.Message;
          }

          return grupoList;
        }
        public List<WorkGroup> GetAllWorkGroupsThisUser(string username, string password)
        {
            List<WorkGroup> grupoList = new List<WorkGroup>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string selectStatement = "SELECT IDWORKGROUP,GROUPNAME FROM USERVIEW WHERE USERNAME = @Username AND PASSWORD = @Password AND ENABLED = 1";
                    using (SqlCommand command = new SqlCommand(selectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@Password", password);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                grupoList.Add(new WorkGroup { IdWorkGroup = reader.GetInt32(0), GroupName = reader.GetString(1) });
                            }
                        }
                    }
                }
            }
            catch { error = true; }

            return grupoList;
        }
        public List<WorkGroup> GetAllWorkGroupsThisUser(string username, string password, int testid)
        {
            List<WorkGroup> grupoList = new List<WorkGroup>();
            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string selectStatement = "SELECT IDWORKGROUP,GROUPNAME FROM USERVIEW WHERE USERNAME = @Username AND PASSWORD = @Password AND TESTID= @TestID AND ENABLED = 1";
                    using (SqlCommand command = new SqlCommand(selectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@Username", username);
                        command.Parameters.AddWithValue("@Password", password);
                        command.Parameters.AddWithValue("@TestID", testid);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                grupoList.Add(new WorkGroup { IdWorkGroup = reader.GetInt32(0), GroupName = reader.GetString(1) });
                            }
                        }
                    }
                }
                return grupoList;
            }
            catch
            {
                error = true;
                return null;   
            }
        }
    #endregion

    #region Users Queries

        public void ChangeUserEnabled(int UserID, bool status)
        {
          try
          {

            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE USERS SET ENABLED = @STATUS WHERE USERID = @ID COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", UserID);
                command.Parameters.AddWithValue("@STATUS", status);

                command.ExecuteScalar();
              }
            }
          }
          catch { error = true; }
        }
        public void ChangeUserOnlineStatus(int UserID, bool status)
          {
            try
            {

              using (SqlConnection connection = dbAccess.GetConnection())
              {
                using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE USERS SET ONLINE = @STATUS, LASTTIME = @FECHA WHERE USERID = @ID COMMIT", connection))
                {
                  command.Parameters.AddWithValue("@ID", UserID);
                  command.Parameters.AddWithValue("@FECHA", DateTime.Now);
                  command.Parameters.AddWithValue("@STATUS", status);

                  command.ExecuteScalar();
                }
              }
            }
            catch { error = true; }
          }

        public void ConfirmDevice(int UserID, string deviceId)
        {
          try
          {

            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE USERS SET NSERIE = @DEVICE WHERE USERID = @ID COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", UserID);
                command.Parameters.AddWithValue("@DEVICE", deviceId);

                command.ExecuteScalar();
              }
            }
          }
          catch { error = true; }
        }

        public bool IsUserMarkedAsOnline(int UserID)
          {
            bool Status = false;
            try
            {
              using (SqlConnection connection = dbAccess.GetConnection())
              {
                using (SqlCommand command = new SqlCommand("SELECT ONLINE FROM USERS WHERE USERID = @ID", connection))
                {
                  command.Parameters.AddWithValue("@ID", UserID);
                  using (SqlDataReader reader = command.ExecuteReader())
                  {
                    while (reader.Read())
                    {
                      Status = reader.GetBoolean(0);
                    }
                  }
                }
              }
            }
            catch { error = true; }
            return Status;
          }

        public User GetAllUserInfoForUserID(int userID)
              {
                  User newUser = null;
                  try
                  {
                      using (SqlConnection connection = dbAccess.GetConnection())
                      {
                          using (SqlCommand command = new SqlCommand("SELECT NOMBRE FROM USERS WHERE USERID = @UserID", connection))
                          {
                              command.Parameters.AddWithValue("@UserID", userID);
                              using (SqlDataReader reader = command.ExecuteReader())
                              {
                                  while (reader.Read())
                                  {
                                      newUser = new User { UserID = userID, Name = reader.GetString(0) };
                                  }
                              }
                          }
                      }
                  }
                  catch { error = true; }

                  return newUser;
              }

        public List<User> GetAllUserList()
        {
            List<User> UserList = new List<User> { };
            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("SELECT USERID, NOMBRE FROM USERS WHERE USERID>0", connection))
                    {
                      
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                User newUser = new User { UserID = reader.GetInt32(0), Name = reader.GetString(1) };
                                UserList.Add(newUser);
                            }
                        }
                    }
                }
            }
            catch { error = true; }

      return UserList;
        }

        public List<User> GetAllActiveUserList(int groupid)
        {
          List<User> UserList = new List<User> { };
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("SELECT USERID, NOMBRE FROM USERS WHERE USERID>0 AND ENABLED=1 AND (IDWORKGROUP=@GID OR @GID=0) ORDER BY NOMBRE", connection))
              {
                command.Parameters.AddWithValue("@GID", groupid);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    User newUser = new User { UserID = reader.GetInt32(0), Name = reader.GetString(1) };
                    UserList.Add(newUser);
                  }
                }
              }
            }
          }
          catch { error = true; }

          return UserList;
        }

        public List<User> GetAllDoctorsThisTest(int testid)
        {
          List<User> UserList = new List<User> { };
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("SELECT USERID, NOMBRE FROM UserView WHERE USERID>0 AND ENABLED=1 AND TESTID=@TID AND IDUSERTYPE=4  ORDER BY NOMBRE", connection))
              {
                command.Parameters.AddWithValue("@TID", testid);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    User newUser = new User { UserID = reader.GetInt32(0), Name = reader.GetString(1) };
                    UserList.Add(newUser);
                  }
                }
              }
            }
          }
          catch { error = true; }

          return UserList;
        }

        public List<User> GetAllDoctorsThisGroup(int groupid)
        {
          List<User> UserList = new List<User> { };
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("SELECT USERID, NOMBRE FROM UserView WHERE USERID>0 AND ENABLED=1 AND IDWORKGROUP=@GID AND IDUSERTYPE=4  ORDER BY NOMBRE", connection))
              {
                command.Parameters.AddWithValue("@GID", groupid);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    User newUser = new User { UserID = reader.GetInt32(0), Name = reader.GetString(1) };
                    UserList.Add(newUser);
                  }
                }
              }
            }
          }
          catch { error = true; }

          return UserList;
        }

        public UserView GetUserViewData(int id)
        {
          UserView UserData = null;
          error = false;

          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("SELECT USERID, IDDOMINIO, DOMINIONAME, IDWORKGROUP, GROUPNAME, TESTID, TESTNAME, IDUSERTYPE, USERTYPE, NOMBRE, USERNAME, PASSWORD,  ENABLED,  ENVIAA, LASTTIME, EQUIPO, NSERIE, LICENCIA, FECHAPOLIZA, FECHAINSTALL, CONTACTO, MAIL, TELEFONO, DIRECCION, NOTAS FROM UserView2   WHERE USERID=@ID ", connection))
              {
                command.Parameters.AddWithValue("@ID", id);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    UserView newUser = new UserView
                    {
                      UserID = reader.GetInt32(0),

                      DomainID = reader.GetInt32(1),
                      Red = reader.GetString(2),

                      GrupoID = reader.GetInt32(3),
                      Grupo = reader.GetString(4),

                      TestID = reader.GetInt32(5),
                      Test = reader.GetString(6),

                      UsertypeID = reader.GetInt32(7),
                      Usertype = reader.GetString(8),

                      Name = reader.GetString(9),
                      Username = reader.GetString(10),
                      Password = reader.GetString(11),

                      Enviaa = "",
                      AssociatedUsersIDs = new List<int>(),

                      Enabled = reader.GetBoolean(12)

                    };

                    string[] associatedStringIDs = new string[] { };
                    string allCommadelUsers;

                    try // Users to send to Set
                    {
                      allCommadelUsers = reader.GetString(13);
                      if (!string.IsNullOrEmpty(allCommadelUsers))
                      {
                        associatedStringIDs = allCommadelUsers.Split(new[] { "," }, StringSplitOptions.None);
                      }

                      foreach (var associatedStringID in associatedStringIDs)
                      {
                        newUser.AssociatedUsersIDs.Add(Int32.Parse(associatedStringID));
                      }

                      newUser.Enviaa = allCommadelUsers;
                    }
                    catch
                    {
                      allCommadelUsers = "";
                      newUser.Enviaa = "";
                    }


                    try { newUser.Lasttime = reader.GetDateTime(14); } catch { newUser.Lasttime = DateTime.MinValue; }

                    try { newUser.Equipo = reader.GetString(15); } catch { newUser.Equipo = ""; }
                    try { newUser.NSerie = reader.GetString(16); } catch { newUser.NSerie = ""; }
                    try { newUser.Licencia = reader.GetString(17); } catch { newUser.Licencia = ""; }

                    try { newUser.Fechapoliza = reader.GetDateTime(18).Date.ToString(); } catch { newUser.Fechapoliza = "1/1/1900"; }
                    try { newUser.FechaInstall = reader.GetDateTime(19).Date.ToString(); } catch { newUser.FechaInstall = "1/1/1900"; }

                    try { newUser.Contacto = reader.GetString(20); } catch { newUser.Contacto = ""; }
                    try { newUser.Email = reader.GetString(21); } catch { newUser.Email = ""; }
                    try { newUser.Telefono = reader.GetString(22); } catch { newUser.Telefono = ""; }
                    try { newUser.Direccion = reader.GetString(23); } catch { newUser.Direccion = ""; }
                    try { newUser.Notas = reader.GetString(24); } catch { newUser.Notas = ""; }


                    UserData = newUser;
                  }
                  
                }
              }
            }
          }

          catch (Exception EX)
          {
            Emensaje = EX.Message;
            error = true;
          }

          return UserData;
        }

        public List<UserView> GetAllUserView(int dominioID, int grupoID)
        {
          List<UserView> UserList = new List<UserView> { };
          error = false;

          try
          {
              using (SqlConnection connection = dbAccess.GetConnection())
              {
                   using (SqlCommand command = new SqlCommand("SELECT USERID, IDDOMINIO, DOMINIONAME, IDWORKGROUP, GROUPNAME, TESTID, TESTNAME, IDUSERTYPE, USERTYPE, NOMBRE, USERNAME, PASSWORD,  ENABLED,  ENVIAA, LASTTIME, NSERIE FROM UserView2   WHERE (USERID>0) AND (IDDOMINIO=@DID OR @DID=0) AND (IDWORKGROUP=@GID OR @GID=0)", connection))
                   {
                        command.Parameters.AddWithValue("@GID", grupoID);
                        command.Parameters.AddWithValue("@DID", dominioID);


                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                  UserView newUser = new UserView
                                  {
                                    UserID = reader.GetInt32(0),

                                    DomainID = reader.GetInt32(1),
                                    Red = reader.GetString(2),

                                    GrupoID = reader.GetInt32(3),
                                    Grupo = reader.GetString(4),

                                    TestID = reader.GetInt32(5),
                                    Test = reader.GetString(6),

                                    UsertypeID = reader.GetInt32(7),
                                    Usertype = reader.GetString(8),

                                    Name = reader.GetString(9),
                                    Username = reader.GetString(10),
                                    Password = reader.GetString(11),

                                    Enviaa = "",
                                    AssociatedUsersIDs = new List<int>(),

                                    Enabled = reader.GetBoolean(12),

                                  };

                                  string[] associatedStringIDs = new string[] { };
                                  string allCommadelUsers;

                                  try // Users to send to Set
                                  {
                                    allCommadelUsers = reader.GetString(13);
                                    if (!string.IsNullOrEmpty(allCommadelUsers))
                                    {
                                      associatedStringIDs = allCommadelUsers.Split(new[] { "," }, StringSplitOptions.None);
                                    }

                                    foreach (var associatedStringID in associatedStringIDs)
                                    {
                                      newUser.AssociatedUsersIDs.Add(Int32.Parse(associatedStringID));
                                    }

                                    newUser.Enviaa = allCommadelUsers;
                                  }
                                  catch
                                  {
                                    allCommadelUsers = "";
                                    newUser.Enviaa = "";
                                  }

                            
                                  try { newUser.Lasttime = reader.GetDateTime(14); } catch { newUser.Lasttime = DateTime.MinValue; }

                                  try { newUser.NSerie = reader.GetString(15); } catch { newUser.NSerie = ""; }



                                  UserList.Add(newUser);

                            }
                       }
                   }
              }
          }
          
              catch (Exception EX)
              {
                Emensaje = EX.Message;
                error = true; 
              }

          return UserList;
        }

        public void DeleteUser(int ID, String USERNAME, String PASSWORD)
        {
          try
          {
            error = false;
            using (SqlConnection connection = dbAccess.GetConnection()) // borrar registro de usuario
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN DELETE USERS WHERE USERID = @ID COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", ID);
                command.ExecuteScalar();
              }
            }

          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
        }

        public void CreateNewUser(UserView thisuser )
        {
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN INSERT INTO USERS (USERNAME,PASSWORD,NOMBRE,IDUSERTYPE,IDWORKGROUP,ENVIAA,ENABLED,EQUIPO,NSERIE,LICENCIA,FECHAPOLIZA,FECHAINSTALL,CONTACTO,MAIL,TELEFONO,DIRECCION,NOTAS ) VALUES (@USERNAME,@PASSWORD,@NOMBRE,@IDUSERTYPE, @IDWORKGROUP,@ENVIAA, @ENABLED,@EQUIPO,@NSERIE,@LICENCIA,@FECHAPOLIZA,@FECHAINSTALL,@CONTACTO,@MAIL,@TELEFONO,@DIRECCION,@NOTAS) COMMIT", connection))
              {
                command.Parameters.AddWithValue("@USERNAME", thisuser.Username);
                command.Parameters.AddWithValue("@PASSWORD", thisuser.Password);
                command.Parameters.AddWithValue("@NOMBRE", thisuser.Name);
                command.Parameters.AddWithValue("@IDUSERTYPE", thisuser.UsertypeID);
                command.Parameters.AddWithValue("@IDWORKGROUP", thisuser.GrupoID);
                command.Parameters.AddWithValue("@ENVIAA", thisuser.Enviaa);
                command.Parameters.AddWithValue("@ENABLED", thisuser.Enabled);

                command.Parameters.AddWithValue("@EQUIPO", thisuser.Equipo);
                command.Parameters.AddWithValue("@NSERIE", thisuser.NSerie);
                command.Parameters.AddWithValue("@LICENCIA", thisuser.Licencia);
                command.Parameters.AddWithValue("@FECHAPOLIZA", thisuser.Fechapoliza);
                command.Parameters.AddWithValue("@FECHAINSTALL", thisuser.FechaInstall);

                command.Parameters.AddWithValue("@CONTACTO", thisuser.Contacto);
                command.Parameters.AddWithValue("@MAIL", thisuser.Email);
                command.Parameters.AddWithValue("@TELEFONO", thisuser.Telefono);
                command.Parameters.AddWithValue("@DIRECCION", thisuser.Direccion);
                command.Parameters.AddWithValue("@NOTAS", thisuser.Notas);
               

                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
        }

        public void UpdateUser(int id, UserView thisuser)
        {
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE USERS SET USERNAME=@USERNAME, PASSWORD=@PASSWORD, NOMBRE=@NOMBRE, IDUSERTYPE=@IDUSERTYPE, IDWORKGROUP=@IDWORKGROUP, ENVIAA=@ENVIAA, ENABLED=@ENABLED, EQUIPO=@EQUIPO, NSERIE=@NSERIE, LICENCIA=@LICENCIA, FECHAPOLIZA=@FECHAPOLIZA, FECHAINSTALL=@FECHAINSTALL, CONTACTO=@CONTACTO, MAIL=@MAIL, TELEFONO=@TELEFONO, DIRECCION=@DIRECCION, NOTAS=@NOTAS WHERE USERID=@ID  COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", id);

                command.Parameters.AddWithValue("@USERNAME", thisuser.Username);
                command.Parameters.AddWithValue("@PASSWORD", thisuser.Password);
                command.Parameters.AddWithValue("@NOMBRE", thisuser.Name);
                command.Parameters.AddWithValue("@IDUSERTYPE", thisuser.UsertypeID);
                command.Parameters.AddWithValue("@IDWORKGROUP", thisuser.GrupoID);
                command.Parameters.AddWithValue("@ENVIAA", thisuser.Enviaa);

                command.Parameters.AddWithValue("@ENABLED", thisuser.Enabled);

                command.Parameters.AddWithValue("@EQUIPO", thisuser.Equipo);
                command.Parameters.AddWithValue("@NSERIE", thisuser.NSerie);
                command.Parameters.AddWithValue("@LICENCIA", thisuser.Licencia);
                command.Parameters.AddWithValue("@FECHAPOLIZA", thisuser.Fechapoliza);
                command.Parameters.AddWithValue("@FECHAINSTALL", thisuser.FechaInstall);

                command.Parameters.AddWithValue("@CONTACTO", thisuser.Contacto);
                command.Parameters.AddWithValue("@MAIL", thisuser.Email);
                command.Parameters.AddWithValue("@TELEFONO", thisuser.Telefono);
                command.Parameters.AddWithValue("@DIRECCION", thisuser.Direccion);
                command.Parameters.AddWithValue("@NOTAS", thisuser.Notas);


                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
    }


    #endregion

    #region Trans Queries

    public bool TransactionExist(string GUID)
        {
          bool found = false;
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("SELECT IDTRANSACTION FROM TRANSACTIONS WHERE TRANSACTIONGUID = @Guid", connection))
              {
                command.Parameters.AddWithValue("@Guid", GUID);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    found = true;
                  }
                }
              }
            }
          }
          catch { error = true; }

          return found;
        }

        public bool TransactionExistForThisPatient(int pID)
        {
          bool found = false;
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("SELECT IDTRANSACTION FROM TRANSACTIONS WHERE PATIENTID = @PID", connection))
              {
                command.Parameters.AddWithValue("@PID", pID);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    found = true;
                  }
                }
              }
            }
          }
          catch { error = true; }
          return found;
        }
        public List<Transaction> GetAllTransactionsSentToThisUser(User user, byte Erased)
        {
            List<Transaction> transactions = new List<Transaction>();

                try
                {
                  using (SqlConnection connection = dbAccess.GetConnection())
                  {
                    const string selectStatement = "SELECT DateTime, PATIENTDATA.PatientID, PatientName, Status, TransType, Nombre, TransactionGuid, ToID, SenderID FROM TRANSACTIONS, PATIENTDATA, STATUS, TRANSACTIONTYPE, USERS WHERE ToID = @ToID AND TRANSACTIONS.StatusID = STATUS.IDStatus AND TRANSACTIONS.TrasType = TRANSACTIONTYPE.IDType AND TRANSACTIONS.PatientID = PATIENTDATA.PatientID AND TRANSACTIONS.SenderID = USERS.UserID AND ERASED=@Erased";
                    using (SqlCommand command = new SqlCommand(selectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@ToID", user.UserID);
                        command.Parameters.AddWithValue("@Erased", Erased);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                transactions.Add(new Transaction 
                                  { Fecha = reader.GetDateTime(0), 
                                    PacienteID = reader.GetInt32(1),
                                    Paciente = reader.GetString(2), 
                                    Status = reader.GetString(3),
                                    Transaccion = reader.GetString(4),
                                    Destinatario = user.Name,
                                    Remitente = reader.GetString(5), 
                                    TransactionGui = reader.GetString(6),
                                    DestinatarioID = reader.GetInt32(7), 
                                    RemitenteID = reader.GetInt32(8)
                                   
                                });
                            }
                        }
                    }
                  }
                }
                catch { error = true; }

                return transactions;
        }

        public List<Transaction> GetAllTransactionsSentByThisUser(User user, byte Erased)
        {
            List<Transaction> transactions = new List<Transaction>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT DateTime, PATIENTDATA.PatientID, PatientName, Status, TransType, Nombre, TransactionGuid, ToID FROM TRANSACTIONS, PATIENTDATA, STATUS, TRANSACTIONTYPE, USERS WHERE SenderID = @SenderID AND TRANSACTIONS.StatusID = STATUS.IDStatus AND TRANSACTIONS.TrasType = TRANSACTIONTYPE.IDType AND TRANSACTIONS.PatientID = PATIENTDATA.PatientID AND TRANSACTIONS.ToID = USERS.UserID AND ERASED=@Erased";
                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@SenderID", user.UserID);
                        command.Parameters.AddWithValue("@Erased", Erased);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                transactions.Add(new Transaction
                                {
                                    Fecha = reader.GetDateTime(0),
                                    PacienteID = reader.GetInt32(1),
                                    Paciente = reader.GetString(2),
                                    Status = reader.GetString(3),
                                    Transaccion = reader.GetString(4),
                                    Remitente = user.Name,
                                    RemitenteID = user.UserID,
                                    Destinatario = reader.GetString(5),
                                    TransactionGui = reader.GetString(6),
                                    DestinatarioID = reader.GetInt32(7)
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return transactions;
        }

        public List<TransactionView> GetAllTransactionsByDate(DateTime from, DateTime to, byte Erased)
        {
            List<TransactionView> transactionsView = new List<TransactionView>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT IDTRANSACTION,TRANSACTIONGUID,DATETIME,PATIENTNAME,TIPOTRANS,STATUS,FROMUSER,TOUSER FROM TransactionView WHERE ((DATETIME>=@from) AND (DATETIME<=@to)) AND (ERASED=@Erased)";
              
                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@from", from);
                        command.Parameters.AddWithValue("@to", to);
                        command.Parameters.AddWithValue("@Erased", Erased);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                               transactionsView.Add(new TransactionView
                                {
                                    IDTrans = reader.GetInt32(0),
                                    TransactionGui = reader.GetString(1),
                                    Fecha = reader.GetDateTime(2),
                                    Paciente = reader.GetString(3),
                                    TrasType = reader.GetString(4),
                                    Status = reader.GetString(5),
                                    Remitente = reader.GetString(6),
                                    Destinatario = reader.GetString(7)
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return transactionsView;
        }

        public List<TransactionView> GetAllTransactionsFilterBy(DateTime from, DateTime to, int sender, int receiver, byte Erased)
        {
            List<TransactionView> transactionsView = new List<TransactionView>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT IDTRANSACTION,TRANSACTIONGUID,DATETIME,PATIENTNAME,TIPOTRANS,STATUS,FROMUSER,TOUSER,TESTNAME FROM TransactionView WHERE ((DATETIME>=@from) AND (DATETIME<=@to)) AND ((SENDERID=@SenderID) OR (@SenderID=0)) AND ((TOID=@ToID) OR (@ToID=0)) AND (ERASED=@Erased)";

                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@from", from);
                        command.Parameters.AddWithValue("@to", to);
                        command.Parameters.AddWithValue("@SenderID", sender);
                        command.Parameters.AddWithValue("@ToID", receiver);
                        command.Parameters.AddWithValue("@Erased", Erased);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                transactionsView.Add(new TransactionView
                                {
                                    IDTrans = reader.GetInt32(0),
                                    TransactionGui = reader.GetString(1),
                                    Fecha = reader.GetDateTime(2),
                                    Paciente = reader.GetString(3),
                                    TrasType = reader.GetString(4),
                                    Status = reader.GetString(5),
                                    Remitente = reader.GetString(6),
                                    Destinatario = reader.GetString(7),
                                    TestName = reader.GetString(8)
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return transactionsView;
        }

        public List<TransactionView> GetAllTransactionsFilterBy(int IDUser, byte Erased)
        {
            List <TransactionView> transactionsView = new List<TransactionView>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT IDTRANSACTION,TRANSACTIONGUID,DATETIME,PATIENTNAME,TIPOTRANS,STATUS,FROMUSER,TOUSER FROM TransactionView WHERE ((SENDERID=@SenderID) OR (@SenderID=0)) AND (ERASED=@Erased)";
                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@SenderID", IDUser);
                        command.Parameters.AddWithValue("@Erased", Erased);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                transactionsView.Add(new TransactionView
                                {
                                    IDTrans = reader.GetInt32(0),
                                    TransactionGui = reader.GetString(1),
                                    Fecha = reader.GetDateTime(2),
                                    Paciente = reader.GetString(3),
                                    TrasType = reader.GetString(4),
                                    Status = reader.GetString(5),
                                    Remitente = reader.GetString(6),
                                    Destinatario = reader.GetString(7)
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return transactionsView;
        }

        public List<Transaction> GetAllNewExamsSentToThisUser(User user, byte All)
        {
            List<Transaction> transactions = new List<Transaction>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT DateTime, PATIENTDATA.PatientID, PatientName, STATUS, TransType, Nombre, TransactionGuid, SenderID FROM TRANSACTIONS, PATIENTDATA, STATUS, TRANSACTIONTYPE, USERS WHERE ToID = @ToID  AND TRANSACTIONS.TrasType =1 AND ((TRANSACTIONS.StatusID = 1) OR (@All=1)) and TRANSACTIONS.StatusID = STATUS.IDStatus AND TRANSACTIONS.TrasType = TRANSACTIONTYPE.IDType AND TRANSACTIONS.PatientID = PATIENTDATA.PatientID AND TRANSACTIONS.SenderID = USERS.UserID AND ERASED=0";
                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@ToID", user.UserID);
                        command.Parameters.AddWithValue("@All", All);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                transactions.Add(new Transaction
                                {
                                    Fecha = reader.GetDateTime(0),
                                    PacienteID = reader.GetInt32(1),
                                    Paciente = reader.GetString(2),
                                    Status = reader.GetString(3),
                                    Transaccion = reader.GetString(4),
                                    Destinatario = user.Name,
                                    DestinatarioID = user.UserID,
                                    Remitente = reader.GetString(5),
                                    TransactionGui = reader.GetString(6),
                                    RemitenteID = reader.GetInt32(7)
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return transactions;
        }

        public List<Transaction> GetAllReceivedExamsSentToThisUser(User user, byte All)
        {
            List<Transaction> transactions = new List<Transaction>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT DateTime, PATIENTDATA.PatientID, PatientName, Status, TransType, Nombre, TransactionGuid, SenderID FROM TRANSACTIONS, PATIENTDATA, STATUS, TRANSACTIONTYPE, USERS WHERE ToID = @ToID AND ((TRANSACTIONS.StatusID =2) OR @All = 1) AND TRANSACTIONS.TrasType =1 AND TRANSACTIONS.StatusID = STATUS.IDStatus AND TRANSACTIONS.TrasType = TRANSACTIONTYPE.IDType AND TRANSACTIONS.PatientID = PATIENTDATA.PatientID AND TRANSACTIONS.SenderID = USERS.UserID AND ERASED=0";
                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@ToID", user.UserID);
                        command.Parameters.AddWithValue("@All", All);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                transactions.Add(new Transaction
                                {
                                    Fecha = reader.GetDateTime(0),
                                    PacienteID = reader.GetInt32(1),
                                    Paciente = reader.GetString(2),
                                    Status = reader.GetString(3),
                                    Transaccion = reader.GetString(4),
                                    Destinatario = user.Name,
                                    DestinatarioID = user.UserID,
                                    Remitente = reader.GetString(5),
                                    TransactionGui = reader.GetString(6),
                                    RemitenteID = reader.GetInt32(7)
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return transactions;
        }

        public List<Transaction> GetAllReceivedExamsSentByThisUser(User user, byte All)
        {
            List<Transaction> transactions = new List<Transaction>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT DateTime, PATIENTDATA.PatientID, PatientName, Status, TransType, Nombre, TransactionGuid, SenderID FROM TRANSACTIONS, PATIENTDATA, STATUS, TRANSACTIONTYPE, USERS WHERE SenderID = @SenderID AND ((TRANSACTIONS.StatusID =2) OR @All = 1) AND TRANSACTIONS.TrasType =1 AND TRANSACTIONS.StatusID = STATUS.IDStatus AND TRANSACTIONS.TrasType = TRANSACTIONTYPE.IDType AND TRANSACTIONS.PatientID = PATIENTDATA.PatientID AND TRANSACTIONS.SenderID = USERS.UserID AND ERASED=0";
                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@SenderID", user.UserID);
                        command.Parameters.AddWithValue("@All", All);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                transactions.Add(new Transaction
                                {
                                    Fecha = reader.GetDateTime(0),
                                    PacienteID = reader.GetInt32(1),
                                    Paciente = reader.GetString(2),
                                    Status = reader.GetString(3),
                                    Transaccion = reader.GetString(4),
                                    Destinatario = reader.GetString(5),
                                    TransactionGui = reader.GetString(6),
                                    DestinatarioID = reader.GetInt32(7),
                                    Remitente = user.Name,
                                    RemitenteID = user.UserID
                                    
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return transactions;
        }

        public List<Transaction> GetAllNewReportsSentToThisUser(User user, byte All)
        {
            List<Transaction> transactions = new List<Transaction>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT DateTime, PATIENTDATA.PatientID, PatientName, Status, TransType, Nombre, TransactionGuid, SenderID FROM TRANSACTIONS, PATIENTDATA, STATUS, TRANSACTIONTYPE, USERS WHERE ToID = @ToID AND TRANSACTIONS.TrasType =2 AND ((TRANSACTIONS.StatusID = 1) OR (@All=1)) AND TRANSACTIONS.StatusID = STATUS.IDSTATUS AND TRANSACTIONS.TrasType = TRANSACTIONTYPE.IDType AND TRANSACTIONS.PatientID = PATIENTDATA.PatientID AND TRANSACTIONS.SenderID = USERS.UserID AND ERASED=0";
                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@ToID", user.UserID);
                        command.Parameters.AddWithValue("@All", All);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                transactions.Add(new Transaction
                                {
                                    Fecha = reader.GetDateTime(0),
                                    PacienteID = reader.GetInt32(1),
                                    Paciente = reader.GetString(2),
                                    Status = reader.GetString(3),
                                    Transaccion = reader.GetString(4),
                                    Destinatario = user.Name,
                                    DestinatarioID = user.UserID,
                                    Remitente = reader.GetString(5),
                                    TransactionGui = reader.GetString(6),
                                    RemitenteID = reader.GetInt32(7)
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return transactions;
        }

        //
        // RECORD A TRANSACTION FOR A NEW PATIENT . CREATE A FRESH PATIENT DATA
        //
        public void RecordTransaction(User fromUser, User toUser, string transGui, int transTypeId, string patientCode, string patientName, string edad, string sexo, string obs)
        {
            try
            {
              using (SqlConnection connection = dbAccess.GetConnection())
              {
                CreateNewPatient(patientCode, patientName, edad, sexo, obs, connection);
                string patientID = ReadPatient(patientCode, patientName, connection);

                using (SqlCommand command = new SqlCommand("BEGIN TRAN INSERT INTO TRANSACTIONS (SenderID, ToID, StatusID, LastChange, PatientID, TransactionGuid, TrasType) VALUES (@SenderID, @ToID, @StatusID, @LastChange, @PatientID, @TransactionGui, @TransType ) COMMIT", connection))
                {
                    command.Parameters.AddWithValue("@SenderID", fromUser.UserID);
                    command.Parameters.AddWithValue("@ToID", toUser.UserID);
                    command.Parameters.AddWithValue("@StatusID", 1);
                    command.Parameters.AddWithValue("@LastChange", DateTime.Now);
                    command.Parameters.AddWithValue("@PatientID", patientID);
                    command.Parameters.AddWithValue("@TransactionGui", transGui);
                    command.Parameters.AddWithValue("@TransType", transTypeId);

                    command.ExecuteScalar();
                }
              }

            }
            catch { error = true; }
        }

        //
        // RECORD A TRANSACTION FOR AN EXISTING PATIENT . NO CHANGE PATIENT DATA
        //
        public void RecordTransaction(User fromUser, User toUser, string transGui, int transTypeId, int patientID )
        {
            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("BEGIN TRAN INSERT INTO TRANSACTIONS (SenderID, ToID, StatusID, LastChange, PatientID, TransactionGuid, TrasType) VALUES (@SenderID, @ToID, @StatusID, @LastChange, @PatientID, @TransactionGui, @TransType ) COMMIT", connection))
                    {
                        command.Parameters.AddWithValue("@SenderID", fromUser.UserID);
                        command.Parameters.AddWithValue("@ToID", toUser.UserID);
                        command.Parameters.AddWithValue("@StatusID", 1);
                        command.Parameters.AddWithValue("@LastChange", DateTime.Now);
                        command.Parameters.AddWithValue("@PatientID", patientID);
                        command.Parameters.AddWithValue("@TransactionGui", transGui);
                        command.Parameters.AddWithValue("@TransType", transTypeId);

                        command.ExecuteScalar();
                    }
                }

            }
            catch { error = true; }
        }

        public void SetTransactionStatus(string transGui, int statusID)
        {
            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE TRANSACTIONS SET StatusID = @StatusID WHERE TransactionGuid = @TransactionGuid COMMIT", connection))
                    {
                        command.Parameters.AddWithValue("@TransactionGuid", transGui);
                        command.Parameters.AddWithValue("@StatusID", statusID);
                        command.ExecuteScalar();
                    }
                }
            }
            catch { error = true; }
        }

        public void SetEraseValue(string transGui, byte Erased)
        {
            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE TRANSACTIONS SET ERASED = @Erased WHERE TransactionGuid = @TransactionGuid COMMIT", connection))
                    {
                        command.Parameters.AddWithValue("@TransactionGuid", transGui);
                        command.Parameters.AddWithValue("@Erased", Erased);
                        command.ExecuteScalar();
                    }
                }
            }
            catch { error = true; }
        }

        public void DeleteTransaction(string transGui)
            {
                try
                {
                    using (SqlConnection connection = dbAccess.GetConnection())
                    {
                        using (SqlCommand command = new SqlCommand("BEGIN TRAN DELETE TRANSACTIONS WHERE TransactionGuid = @TransactionGuid COMMIT", connection))
                        {
                            command.Parameters.AddWithValue("@TransactionGuid", transGui);
                            command.ExecuteScalar();
                        }
                    }
                }
                catch { error = true; }
            }

    #endregion 

    #region Installations Actions
        public void DesactivateInstallation(int IDInstall)
        {
            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE INSTALLATIONS SET INSTALLED = 0  WHERE IDINSTALL = @ID COMMIT", connection))
                    {
                        command.Parameters.AddWithValue("@ID", IDInstall);
                        command.ExecuteScalar();
                    }
                }
            }
            catch { error = true; }
        }

        public void RecordInstallationData (Installation install)
        {
            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE INSTALLATIONS SET INSTALLED = 1, INSTALLDATE = @FECHA, UBICACION = @UBICACION, MACHINEID = @MACHINEID WHERE IDINSTALL = @ID COMMIT", connection))
                    {
                        command.Parameters.AddWithValue("@ID", install.IDInstall);
                        command.Parameters.AddWithValue("@FECHA", install.InstallDate);
                        command.Parameters.AddWithValue("@UBICACION", install.Ubicacion);
                        command.Parameters.AddWithValue("@MACHINEID", install.MachineName);

                        command.ExecuteScalar();
                    }
                }
            }
            catch { error = true; }
        }

        public Installation GetInstallationData(int InstallID)
        {
            Installation InstData = null;

            try
            {
               using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string SelectStatement = "SELECT USERNAME,PASSW,ACTKEY,UBICACION,INSTALLED,INSTALLDATE,MACHINEID,SERIAL,IDUSERTYPE,TESTID,CODEID FROM INSTALLATIONS WHERE IDINSTALL = @ID";
                    using (SqlCommand command = new SqlCommand(SelectStatement, connection))
                    {
                        command.Parameters.AddWithValue("@ID", InstallID);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                InstData = new Installation
                                {
                                    IDInstall = InstallID,
                                    UserName = reader.GetString(0),
                                    Passw = reader.GetString(1),
                                    ActKey = reader.GetString(2),
                                    Ubicacion = reader.GetString(3),
                                    Installed = reader.GetBoolean(4),
                                    InstallDate = reader.GetDateTime(5).Date,
                                    MachineName = reader.GetString(6),
                                    DeviceSerial = reader.GetString(7),
                                    UserType = reader.GetInt32(8),
                                    TestID = reader.GetInt32(9),
                                    CodeID = reader.GetInt32(10)
                                };
                            }
                        }
                    }
                 }
            }


            catch (Exception EX)
            {
                error = true;
                throw EX;
            }

            return InstData;
        }

        public List<Installation> GetInstallationsthisUser(string user, string password, int testid)
        {
         List<Installation> InstList = new List<Installation> { };

          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              const string SelectStatement = "SELECT IDINSTALL,USERNAME,PASSW,ACTKEY,UBICACION,INSTALLED,INSTALLDATE,MACHINEID,SERIAL,IDUSERTYPE,TESTID,CODEID FROM INSTALLATIONS WHERE USERNAME = @USER AND PASSW=@PASS AND TESTID=@TID";
              using (SqlCommand command = new SqlCommand(SelectStatement, connection))
              {
                command.Parameters.AddWithValue("@USER", user);
                command.Parameters.AddWithValue("@PASS", password);
                command.Parameters.AddWithValue("@TID", testid);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    Installation InstData = new Installation
                    {
                      IDInstall = reader.GetInt32(0),
                      UserName = reader.GetString(1),
                      Passw = reader.GetString(2),
                      ActKey = reader.GetString(3),
                      Ubicacion = reader.GetString(4),
                      Installed = reader.GetBoolean(5),
                      InstallDate = reader.GetDateTime(6),
                      MachineName = reader.GetString(7),
                      DeviceSerial = reader.GetString(8),
                      UserType = reader.GetInt32(9),
                      TestID = reader.GetInt32(10),
                      CodeID = reader.GetInt32(11)
                    };

                    InstList.Add(InstData);
                  }
                }
              }
            }
          }


          catch (Exception EX)
          {
            error = true;
            throw EX;
          }

          return InstList;
        }

        public void DeleteInstallation(int InstallID)
        {
          try
          {
            error = false;
            
            // DELETE this  INSTALATIONS ID onliy

            using (SqlConnection connection = dbAccess.GetConnection()) // borrar registro de usuario
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN DELETE INSTALLATIONS WHERE IDINSTALL=@ID COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", InstallID);
                command.ExecuteScalar();
              }
            }

          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
        }

        public void CreateNewInstall(Installation thisuser)
        {
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN INSERT INTO INSTALLATIONS (USERNAME,PASSW,ACTKEY,UBICACION,INSTALLED,MACHINEID,SERIAL,IDUSERTYPE,TESTID,CODEID) VALUES (@USERNAME,@PASSW,@ACTKEY,@UBICACION,@INSTALLED,@MACHINEID,@SERIAL,@IDUSERTYPE,@TESTID,@CODEID) COMMIT", connection))
              {
                command.Parameters.AddWithValue("@USERNAME", thisuser.UserName);
                command.Parameters.AddWithValue("@PASSW", thisuser.Passw);
                command.Parameters.AddWithValue("@ACTKEY", thisuser.ActKey);
                command.Parameters.AddWithValue("@UBICACION", thisuser.Ubicacion);
                command.Parameters.AddWithValue("@INSTALLED", thisuser.Installed);
                command.Parameters.AddWithValue("@MACHINEID", thisuser.MachineName);
                command.Parameters.AddWithValue("@SERIAL", thisuser.DeviceSerial);

                command.Parameters.AddWithValue("@IDUSERTYPE", thisuser.UserType);
                command.Parameters.AddWithValue("@TESTID", thisuser.TestID);
                command.Parameters.AddWithValue("@CODEID", thisuser.CodeID);
            
                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
        }

        public void UpdateInstall(int id, Installation thisuser)
        {
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
                using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE INSTALLATIONS SET USERNAME=@USERNAME, PASSW=@PASSW, ACTKEY=@ACTKEY, UBICACION=@UBICACION, INSTALLED=@INSTALLED, MACHINEID=@MACHINEID, SERIAL=@SERIAL, IDUSERTYPE=@IDUSERTYPE, TESTID=@TESTID, CODEID=@CODEID WHERE IDINSTALL=@ID COMMIT", connection))
                {
                  command.Parameters.AddWithValue("@ID", id);

                  command.Parameters.AddWithValue("@USERNAME", thisuser.UserName);
                  command.Parameters.AddWithValue("@PASSW", thisuser.Passw);
                  command.Parameters.AddWithValue("@ACTKEY", thisuser.ActKey);
                  command.Parameters.AddWithValue("@UBICACION", thisuser.Ubicacion);
                  command.Parameters.AddWithValue("@INSTALLED", thisuser.Installed);
                  command.Parameters.AddWithValue("@MACHINEID", thisuser.MachineName);
                  command.Parameters.AddWithValue("@SERIAL", thisuser.DeviceSerial);

                  command.Parameters.AddWithValue("@IDUSERTYPE", thisuser.UserType);
                  command.Parameters.AddWithValue("@TESTID", thisuser.TestID);
                  command.Parameters.AddWithValue("@CODEID", thisuser.CodeID);

                  command.ExecuteScalar();
                }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
        }

    #endregion

    #region Patients Actions
    public string ReadPatient(string patientCode, string patientName, SqlConnection connection)
        {
            string patientID = String.Empty;

            try
            {
                using (SqlCommand command = new SqlCommand("SELECT PatientID FROM PATIENTDATA WHERE PATIENTCODE = @PATIENTCODE AND PATIENTNAME = @PATIENTNAME", connection))
                {
                    command.Parameters.AddWithValue("@PATIENTCODE", patientCode);
                    command.Parameters.AddWithValue("@PATIENTNAME", patientName);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())  // SI HAY MAS DE UNO TOMA EL ULTIMO
                        {
                            patientID = reader.GetInt32(0).ToString();
                        }
                    }
                }
            }
            catch { error = true; }
            return patientID;
        }
                      
        public string GetPatientCodeByID(int patientID)
        {
            string patientCode = string.Empty;

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (
                        SqlCommand command =
                            new SqlCommand("SELECT PATIENTCODE, PATIENTNAME FROM PATIENTDATA WHERE PATIENTID= @PATIENTID", connection))
                    {
                        command.Parameters.AddWithValue("@PATIENTID", patientID);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                patientCode = reader.GetString(0);
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return patientCode;
        }

        public PatientData GetPatientData(int patientID)
        {
            PatientData pdata = null;

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (
                        SqlCommand command =
                            new SqlCommand("SELECT PATIENTID, PATIENTCODE, PATIENTNAME, EDAD, SEXO, OBS FROM PATIENTDATA WHERE PATIENTID= @PATIENTID",
                                           connection))
                    {
                        command.Parameters.AddWithValue("@PATIENTID", patientID);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                pdata = new PatientData
                                {
                                    PID = reader.GetInt32(0),
                                    PCode = reader.GetString(1),
                                    PName = reader.GetString(2),
                                    Edad = reader.GetString(3),
                                    Sexo = reader.GetString(4),
                                    Obs = reader.GetString(5)
                                };

                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return pdata;
        }

        public List<PatientData> GetAllPatients()
        {
            List<PatientData> patients = new List<PatientData>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string selectStatement = "SELECT PATIENTID, PATIENTCODE, PATIENTNAME FROM PATIENTDATA";
                    using (SqlCommand command = new SqlCommand(selectStatement, connection))
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                patients.Add(new PatientData
                                {
                                    PID = reader.GetInt32(0),
                                    PCode = reader.GetString(1),
                                    PName = reader.GetString(2),
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return patients;
        }

        public List<PatientData> GetAllPatientsData()
        {
            List<PatientData> patients = new List<PatientData>();

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    const string selectStatement = "SELECT PATIENTID, PATIENDCODE, PATIENTNAME, EDAD, SEXO, OBS FROM PATIENTDATA";
                    using (SqlCommand command = new SqlCommand(selectStatement, connection))
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                patients.Add(new PatientData
                                {
                                    PID = reader.GetInt32(0),
                                    PCode = reader.GetString(1),
                                    PName = reader.GetString(2),
                                    Edad = reader.GetString(3),
                                    Sexo = reader.GetString(4),
                                    Obs = reader.GetString(5)
                                });
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return patients;
        }

        public void CreateNewPatient(string patientCode, string patientName, string edad, string sexo, string obs, SqlConnection connection)
        {
            try
            {
                using (SqlCommand command = new SqlCommand("BEGIN TRAN INSERT INTO PATIENTDATA (PATIENTCODE,PATIENTNAME,EDAD,SEXO,OBS) VALUES (@PATIENTCODE,@PATIENTNAME,@EDAD,@SEXO,@OBS) COMMIT", connection))
                {
                    command.Parameters.AddWithValue("@PATIENTCODE", patientCode);
                    command.Parameters.AddWithValue("@PATIENTNAME", patientName);
                    command.Parameters.AddWithValue("@EDAD", edad);
                    command.Parameters.AddWithValue("@SEXO", sexo);
                    command.Parameters.AddWithValue("@OBS", obs);
                    command.ExecuteScalar();
                }
            }
            catch { error = true; }
        }

        public void DeletePatient(int ID)
    {
      try
      {
        using (SqlConnection connection = dbAccess.GetConnection())
        {
          using (SqlCommand command = new SqlCommand("BEGIN TRAN DELETE PATIENTDATA WHERE PATIENTID = @ID COMMIT", connection))
          {
            command.Parameters.AddWithValue("@ID", ID);
            command.ExecuteScalar();
          }
        }
      }
      catch { error = true; }
    }

    #endregion

    #region Others Actions
        public string GetMailForUser(int userID)
        {
            string mail  = string.Empty;

            try
            {
                using (SqlConnection connection = dbAccess.GetConnection())
                {
                    using (SqlCommand command = new SqlCommand("SELECT MAIL FROM USERS WHERE USERID = @UserID", connection))
                    {
                        command.Parameters.AddWithValue("@UserID", userID);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                try { mail = reader.GetString(0); }
                                catch { } // do nothing
                            }
                        }
                    }
                }
            }
            catch { error = true; }
            return mail;
        }

        public void SendMail(string From, string Passw, string To, string txtSubject, string txtBody, List<string> AttachFiles)
        {
            try
            {
                SmtpClient SMTPServer = new SmtpClient("smtpout.secureserver.net", 80);
                SMTPServer.Credentials = new NetworkCredential
                {
                    UserName = From,
                    Password = Passw,
                };

                MailMessage mailObj = new MailMessage(From, To, txtSubject, txtBody);

                if (AttachFiles != null)  // there are attachments
                {
                    foreach (string fn in AttachFiles)
                    {
                        Attachment a = new Attachment(fn);
                        mailObj.Attachments.Add(a);
                    }
                }
                
                SMTPServer.Send(mailObj);
            }
            catch { error = true; }
        }
                    
        public void NotifyUser(User fromUser, User toUser, int TransTypeId)
        {
            string mail = GetMailForUser(toUser.UserID);
            if (mail.Length>4) // if there is mail string send notification
            {
                string envio;
                if (TransTypeId == 1) envio = "Examen"; else envio = "Informe";
                SendMail("notificaciones@medlinknet.com","pnotificaciones",mail,"Notificacion de "+envio+" enviado por "+fromUser.Name,fromUser.Name+" le ha sido enviado un "+envio+" por la Red Medlinknet, favor revisarlo a la brevedad posible",null);
            }
        }
    #endregion

    #region Catalogos
        public List<Domain> GetDomainsList()
        {
          List<Domain> domains = new List<Domain>();

          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
             
              using (SqlCommand command = new SqlCommand("SELECT IDDOMINIO, DOMINIONAME, DESCRIPTION, ALIAS  FROM DOMINIOS WHERE IDDOMINIO>0", connection))
              {

                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    Domain item = new Domain
                    {
                      IDDOMINIO = reader.GetInt32(0),
                      DOMINIONAME = reader.GetString(1),
                      DESCRIPTION = reader.GetString(2),
                      ALIAS = reader.GetString(3)

                    };
                    domains.Add(item);
                    
                  }
                }
              }
            }
          }
          catch ( Exception ex )
               { 
                  error = true;  
                  Emensaje = ex.Message;
                }
          return domains;
        }
        public List<UserType> GetUserTypeList()
        {
          List<UserType> TypeList = new List<UserType>();

          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {

              using (SqlCommand command = new SqlCommand("SELECT IDUSERTYPE, USERTYPE FROM USERTYPES WHERE IDUSERTYPE>0", connection))
              {

                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    UserType item = new UserType
                    {
                      IDUSERTYPE = reader.GetInt32(0),
                      USERTYPE = reader.GetString(1),
                    };
                    TypeList.Add(item);

                  }
                }
              }
            }
          }
          catch (Exception ex)
          {
            error = true;
            Emensaje = ex.Message;
          }
          return TypeList;
        }

        public List<Role> GetRoleList()
        {
          List<Role> RoleList = new List<Role>();

          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {

              using (SqlCommand command = new SqlCommand("SELECT IDROLE, ROLENAME FROM ROLES WHERE IDROLE>0", connection))
              {

                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    Role item = new Role
                    {
                      IDROLE = reader.GetInt32(0),
                      ROLENAME = reader.GetString(1),
                    };
                    RoleList.Add(item);

                  }
                }
              }
            }
          }
          catch (Exception ex)
          {
            error = true;
            Emensaje = ex.Message;
          }
          return RoleList;
        }

        public string GetTestName(int id)
        {
          string name = "";
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {

              using (SqlCommand command = new SqlCommand("SELECT TESTNAME FROM TEST WHERE TESTID = @TID", connection))
              {
                command.Parameters.AddWithValue("@TID", id);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    name = reader.GetString(0);
                  }
                }
              }
            }
          }
          catch (Exception ex)
          {
            error = true;
            Emensaje = ex.Message;
          }
          return name;
        }

        public Test GetTest(int id)
        {
         Test test = null;
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {

              using (SqlCommand command = new SqlCommand("SELECT TESTID, TESTNAME FROM TEST WHERE TESTID = @TID", connection))
              {
                command.Parameters.AddWithValue("@TID", id);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                     test = new Test { TESTID = reader.GetInt32(0), TESTNAME = reader.GetString(1) } ;
                  }
                }
              }
            }
          }
          catch (Exception ex)
          {
            error = true;
            Emensaje = ex.Message;
          }
          return test;
        }

        public WorkGroup GetGroup(int id)
        {
          WorkGroup wgroup = null;
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {

              using (SqlCommand command = new SqlCommand("SELECT IDWORKGROUP, DESCRIPTION, IDDOMINIO, IDTEST FROM WORKGROUP WHERE IDWORKGROUP = @ID", connection))
              {
                command.Parameters.AddWithValue("@ID", id);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    wgroup = new WorkGroup { IdWorkGroup = reader.GetInt32(0), GroupName = reader.GetString(1), IdDominio = reader.GetInt32(2), IdTest = reader.GetInt32(3) };
                  }
                }
              }
            }
          }
          catch (Exception ex)
          {
            error = true;
            Emensaje = ex.Message;
          }
          return wgroup;
        }

        public UserType GetUserType(int id)
        {
          UserType utype = null;
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {

              using (SqlCommand command = new SqlCommand("SELECT IDUSERTYPE, USERTYPE FROM USERTYPES WHERE IDUSERTYPE = @ID", connection))
              {
                command.Parameters.AddWithValue("@ID", id);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    utype = new UserType { IDUSERTYPE = reader.GetInt32(0), USERTYPE = reader.GetString(1) };
                  }
                }
              }
            }
          }
          catch (Exception ex)
          {
            error = true;
            Emensaje = ex.Message;
          }
          return utype;
        }

        public Role GetRole(int id)
        {
          Role utype = null;
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {

              using (SqlCommand command = new SqlCommand("SELECT IDROLE, ROLENAME FROM ROLES WHERE IDROLE = @ID", connection))
              {
                command.Parameters.AddWithValue("@ID", id);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                  while (reader.Read())
                  {
                    utype = new Role { IDROLE = reader.GetInt32(0), ROLENAME = reader.GetString(1) };
                  }
                }
              }
            }
          }
          catch (Exception ex)
          {
            error = true;
            Emensaje = ex.Message;
          }
          return utype;
        }

        public void CreateNewDomain (Domain domain)
        {
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN INSERT INTO DOMINIOS (DOMINIONAME,DESCRIPTION,ALIAS) VALUES (@DOMINIONAME,@DESCRIPTION,@ALIAS)  COMMIT", connection))
              {
                command.Parameters.AddWithValue("@DOMINIONAME", domain.DOMINIONAME);
                command.Parameters.AddWithValue("@DESCRIPTION", domain.DESCRIPTION);
                command.Parameters.AddWithValue("@ALIAS", domain.ALIAS);

                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }

        }

        public void CreateNewWorkGroup(WorkGroup grupo)
        {
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN INSERT INTO WORKGROUP (DESCRIPTION,IDDOMINIO,IDTEST, DISKNAME, JDENABLED, AWSENABLED) VALUES (@DESCRIPTION,@IDDOMINIO,@IDTEST,@DISKNAME, @JDENABLED,@AWSENABLED)  COMMIT", connection))
              {
                command.Parameters.AddWithValue("@DESCRIPTION", grupo.GroupName);
                command.Parameters.AddWithValue("@IDDOMINIO", grupo.IdDominio);
                command.Parameters.AddWithValue("@IDTEST", grupo.IdTest);

                command.Parameters.AddWithValue("@DISKNAME", "");
                command.Parameters.AddWithValue("@JDENABLED", 0);
                command.Parameters.AddWithValue("@AWSENABLED", 1); // setup to amazon

                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }

        }

        public void UpdateDomain(Domain domain)
        {
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE DOMINIOS  SET DOMINIONAME=@DOMINIONAME, DESCRIPTION=@DESCRIPTION, ALIAS=@ALIAS WHERE IDDOMINIO=@ID COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", domain.IDDOMINIO);
                command.Parameters.AddWithValue("@DOMINIONAME", domain.DOMINIONAME);
                command.Parameters.AddWithValue("@DESCRIPTION", domain.DESCRIPTION);
                command.Parameters.AddWithValue("@ALIAS", domain.ALIAS);

                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }

        }

        public void UpdateWorkGroup(WorkGroup grupo)
        {
          try
          {
            using (SqlConnection connection = dbAccess.GetConnection())
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN UPDATE WORKGROUP SET DESCRIPTION=@DESCRIPTION, IDDOMINIO=@IDDOMINIO, IDTEST=@IDTEST WHERE IDWORKGROUP=@ID  COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", grupo.IdWorkGroup);
                command.Parameters.AddWithValue("@DESCRIPTION", grupo.GroupName);
                command.Parameters.AddWithValue("@IDDOMINIO", grupo.IdDominio);
                command.Parameters.AddWithValue("@IDTEST", grupo.IdTest);

                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }

        }

        public void DeleteWorkGroup(int ID)
        {
          try
          {
            error = false;
            using (SqlConnection connection = dbAccess.GetConnection()) // borrar registro de usuario
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN DELETE WORKGROUP WHERE IDWORKGROUP = @ID COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", ID);
                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
        }

        public void DeleteDomain(int ID)
        {
          try
          {
            error = false;
            using (SqlConnection connection = dbAccess.GetConnection()) // borrar registro de usuario
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN DELETE DOMINIOS WHERE IDDOMINIO = @ID COMMIT", connection))
              {
                command.Parameters.AddWithValue("@ID", ID);
                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
        }

        public void DeleteOldPatients()
        {
          try
          {
            error = false;
            using (SqlConnection connection = dbAccess.GetConnection()) // borrar registro de usuario
            {
              using (SqlCommand command = new SqlCommand("BEGIN TRAN EXEC DELETEPATIENTS COMMIT", connection))
              {
                command.ExecuteScalar();
              }
            }
          }
          catch (Exception Ex)
          {
            error = true;
            Emensaje = Ex.Message;
            throw new System.ArgumentException(Emensaje);
          }
        }


    #endregion

  }



}
