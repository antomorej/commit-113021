﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
    [Serializable]
    public class Installation
    {
        public int  IDInstall
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string Passw
        {
            get;
            set;
        }

        public string ActKey
        {
            get;
            set;
        }

        public string Ubicacion
        {
            get;
            set;
        }

        public bool Installed
        {
            get;
            set;
        }

        public DateTime InstallDate
        {
            get;
            set;
        }

        public string MachineName
        {
            get;
            set;
        }

        public int UserType
        {
            get;
            set;
        }

        public int TestID
        {
            get;
            set;
        }

        public int CodeID
        {
            get;
            set;
        }

        public string DeviceSerial
        {

            get
            {
              // Unir DeviceType y Serial
              return DeviceType + "-" + Serial;
            }

            set 
            {
                // Split DeviceType and Serial comming as a hole from DeviceSerial

                string str = value;
                int pos = str.IndexOf("-");

                if (pos > 0)
                {
                    DeviceType = str.Substring(0, pos);  // Give Device Type
                    Serial = str.Substring(pos + 1, str.Length - pos - 1);
                }
                else
                {
                    DeviceType = "N/A";    // Type No available
                    Serial = str;
                }
            }
            
        }

        public string Serial
        {
            get;
            set;
        }

        public string DeviceType
        {
            get;
            set;
        }
        
    }
}
