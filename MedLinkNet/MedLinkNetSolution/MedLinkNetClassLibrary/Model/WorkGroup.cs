﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
    public class WorkGroup
    {
        public int  IdWorkGroup
        {
            get;
            set;
        }

        public string GroupName
        {
            get; 
            set;
        }

        public int IdDominio
        {
          get;
          set;
        }

        public int IdTest
        {
          get;
          set;
        }

        public override string ToString()
        {
            return IdWorkGroup + " - " + GroupName ;
        }
    }
}
