﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
    public class Domain
    {
        public int  IDDOMINIO
        {
            get;
            set;
        }


    public string DOMINIONAME
    {
      get;
      set;
    }

    public string DESCRIPTION
    {
      get;
      set;
    }

    public string ALIAS
    {
      get;
      set;
    }

    public override string ToString()
        {
            return IDDOMINIO + " - " + DOMINIONAME ;
        }
    }
}
