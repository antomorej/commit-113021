﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
    public class Test
    {
        public int  TESTID
        {
            get;
            set;
        }


        public string TESTNAME
        {
          get;
          set;
        }


        public override string ToString()
            {
              return TESTID  + " - " + TESTNAME;
            }
        }
}
