﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedLinkNet.Model
{
    public class Role
    {
        public int  IDROLE
        {
            get;
            set;
        }


        public string ROLENAME
        {
          get;
          set;
        }


        public override string ToString()
            {
              return IDROLE  + " - " + ROLENAME;
            }
        }
}
