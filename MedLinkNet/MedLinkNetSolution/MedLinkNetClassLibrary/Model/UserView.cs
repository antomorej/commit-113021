﻿using System;
using System.Collections.Generic;
using MedLinkNet.Model.Connection;

namespace MedLinkNet.Model
{
    public class UserView
    {
        public int UserID
        {
            get;
            set;
        }

        public int DomainID
        {
          get;
          set;
        }
        public string Red
        {
          get;
          set;
        }

        public int GrupoID
        {
          get;
          set;
        }

        public string Grupo { get; set; }

        public int TestID { get; set; }

        public string Test
        {
          get;
          set;
        }

        public int UsertypeID
        {
          get;
          set;
        }

        public string Usertype
        {
          get;
          set;
        }
        public string Name
        {
          get; set;
        }

        public string Username
        {
            get;
            set;
        }

        public string Password
        {
          get;
          set;
        }

        public string Enviaa
        {
          get;
          set;
        }

       public List<int> AssociatedUsersIDs
        {
            get; 
            set;
        }

        public bool Enabled
        {
          get;
          set;
        }

        public DateTime Lasttime
        {
          get;
          set;
        }

        public bool Online
        {
          get
          {
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            return dataAccessLayer.IsUserMarkedAsOnline(this.UserID);
          }
          set
          {
            DataAccessLayer dataAccessLayer = DataAccessLayer.GetDataAccessLayer();
            dataAccessLayer.ChangeUserOnlineStatus(this.UserID, value);
          }
        }

        public string Equipo { get; set; }
        public string NSerie { get; set; }
        public string Licencia { get; set; }
        public String Fechapoliza { get; set; }
        public String FechaInstall { get; set; }

        public string Contacto { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Notas { get; set; }


    public override string ToString()
        {
            return UserID + " - " + Name;
        }
    }
}
