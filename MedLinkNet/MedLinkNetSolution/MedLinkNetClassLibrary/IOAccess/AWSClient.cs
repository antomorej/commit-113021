﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace MedLinkNet.IOAccess
{

  public class MedLinkNetClient
    {
        public AmazonS3Exception OperationEx;

        static AmazonS3 S3client;    // Client connection ONLY 1;

        public long TransferredBytes = 0;   // Data during Upload or Download Process
        public long TotalBytes = 0;
        public double AcumMb = 0;
        public string trace;
        public bool FolderFound;

    // To Take Keys from Properties
    //
        private readonly string MyAccessKeyID = Properties.Settings.Default.AccessKeyID;  //"AKIAJQ2GNPCBHZECVX2Q";
        private readonly string MySecretKeyID = Properties.Settings.Default.SecretKeyID;  // "fU1doT3GBncrwmIBtURSindpgw3Qsaeuj/xZ68U6";
        public string MyBucketName = Properties.Settings.Default.AWSBucket;  // "medlinknet";
       
        public readonly string TempPath = Properties.Settings.Default.TempPath; // "C:/ MedLinkNet / TempData /";

         public event EventHandler OnProgress; // optional event 

        public bool ApplyCompress = false;  // Indicate if Compress will apply

        // Connect to AWS service
        public void Connect(string accessKeyID, string secretKeyID)
        {
            // Create AWS Client if not exist yet
            if (S3client == null)
             {
               ServicePointManager.DefaultConnectionLimit = 100;
               S3client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKeyID, secretKeyID);
               //  S3client = new Amazon.S3.AmazonS3Client(accessKeyID, secretKeyID);
             }
        }

        bool compressed = false; // Indicate if file wa compressed

        public string TempFile(string FileName)
        {
            if (!Directory.Exists(TempPath)) Directory.CreateDirectory(TempPath);
            return (TempPath + FileName);      
        }

        private void CompressLogic(ref string sourceFileName, ref string keyName)
        {
           FileInfo finfo = new FileInfo(sourceFileName);

           if ((ApplyCompress) & (finfo.Length > 512000)) // 500 kb
           {
               string TFile = TempFile(finfo.Name);
               // Compress file to the TempPath adding CompressExt (."cmp")
               CompressFunctions.Compress(sourceFileName, TFile, true);
               compressed = true;

               // Change sourceFileName by TempFile and Add .cmp to keyname
               sourceFileName = TFile + CompressFunctions.CompressExt;
               keyName = keyName + CompressFunctions.CompressExt;
           }
           else compressed = false;
        }
  
        public void PutFile(string keyName, string sourceFileName)
        {
            PutFile(MyAccessKeyID, MySecretKeyID, MyBucketName, keyName, sourceFileName);
        }
        private void PutFile(string AccessKeyID, string SecretKeyID, string bucketName, string keyName, string sourceFileName)
        {
            Connect(AccessKeyID, SecretKeyID); // create S3client
            
            // New PutObject request
            PutObjectRequest request = new PutObjectRequest();

            CompressLogic(ref sourceFileName, ref keyName);

            // Build  request attributes
            request.WithBucketName(bucketName);
            request.WithFilePath(sourceFileName);
            request.WithKey(keyName);

            // Call request
            S3client.PutObject(request);

            // delete temp file if compressed
            if (compressed) File.Delete(sourceFileName);
        }

        // Upload a File using High Level Multipart
        //
        public void TransferFile(string keyName, string sourceFileName)
        {
            TransferFile(MyAccessKeyID, MySecretKeyID, "mediventa", keyName, sourceFileName);
        }
        private void TransferFile(string accessKeyID, string secretKeyID, string bucketName, string keyName, string sourceFileName)
        {
            try
            {
                FileInfo finfo = new FileInfo(sourceFileName);
                trace = finfo.Name;

                ServicePointManager.DefaultConnectionLimit = 100;

                TransferUtilityConfig config = new TransferUtilityConfig();
                config.DefaultTimeout = 3600000; // 1 hour
                TransferUtility utility = new TransferUtility(accessKeyID, secretKeyID, config);

                CompressLogic(ref sourceFileName, ref keyName);

                TransferUtilityUploadRequest uploadRequest = new TransferUtilityUploadRequest()
                    .WithBucketName(bucketName)
                    .WithFilePath(sourceFileName)
                    .WithStorageClass(S3StorageClass.ReducedRedundancy)
                //    .WithPartSize(8*1048576) // This is 1 MB. 
                    .WithKey(keyName)
                    .WithCannedACL(S3CannedACL.PublicRead);
                uploadRequest.UploadProgressEvent += new EventHandler<UploadProgressArgs>(TransferPartProgressEvent);

                // execute Upload
                
                utility.Upload(uploadRequest);

                // delete temp file if compressed
                if (compressed) File.Delete(sourceFileName);

            }

            catch (AmazonS3Exception ex)
            {
                trace = "EX" + trace;
                throw ex; 
            }
            
        }

        // Download a File using Tranfer Utility
        //
        public void DownLoadFile(string keyName, string destFileName)
        {
            DownLoadFile(MyAccessKeyID, MySecretKeyID, MyBucketName, keyName, destFileName);
        }
        private void DownLoadFile(string accessKeyID, string secretKeyID, string bucketName, string keyName, string destFileName)
        {
            bool ExistPrevious;
            DateTime dateCreated = new DateTime();
            DateTime dateModified = new DateTime();

            try
            {
                FileInfo Traceinfo = new FileInfo(destFileName);
                trace = Traceinfo.Name;

                if (destFileName.Contains(CompressFunctions.CompressExt))
                {
                    // Remove CompressExt from destFileName
                    int pos = destFileName.IndexOf(CompressFunctions.CompressExt);
                    int longExt = CompressFunctions.CompressExt.Length;
                    destFileName = destFileName.Substring(0, destFileName.Length - longExt);
                }

                ExistPrevious = File.Exists(destFileName);
               
                if (ExistPrevious)    // Take Dates of destfile
                {
                    dateCreated = File.GetCreationTime(destFileName);
                    dateModified = File.GetLastWriteTime(destFileName);
                }
                
                ServicePointManager.DefaultConnectionLimit = 100;
                TransferUtilityConfig config = new TransferUtilityConfig();
                config.DefaultTimeout = 3600000; // 1 hour
                TransferUtility utility = new TransferUtility(accessKeyID, secretKeyID, config);


                if (keyName.Contains(CompressFunctions.CompressExt)) // need to be descompressed
                {
                    FileInfo finfo = new FileInfo(destFileName);
                    string TFile = TempFile(finfo.Name);
                    TransferUtilityDownloadRequest downloadRequest = new TransferUtilityDownloadRequest()
                       .WithBucketName(bucketName)
                       .WithFilePath(TFile)
                       .WithKey(keyName);
                    downloadRequest.WriteObjectProgressEvent += new EventHandler<WriteObjectProgressArgs>(DownPartProgressEvent);
                    utility.Download(downloadRequest); // to TempFile firts
                    CompressFunctions.Decompress(TFile, destFileName);
                    File.Delete(TFile);  // delete temp file after descompress
                }
                else // Directo
                {
                    TransferUtilityDownloadRequest downloadRequest = new TransferUtilityDownloadRequest()
                        .WithBucketName(bucketName)
                        .WithFilePath(destFileName)
                        .WithKey(keyName);
                    downloadRequest.WriteObjectProgressEvent += new EventHandler<WriteObjectProgressArgs>(DownPartProgressEvent);
                    utility.Download(downloadRequest);
                }

                // Restore Dates if ExistPrevious
                if (ExistPrevious)    // Restore Dates of destfile
                {
                    File.SetCreationTime(destFileName, dateCreated);
                    File.SetLastWriteTime(destFileName, dateModified);
                }

            }
            catch (AmazonS3Exception ex)
            {
                trace = "EX" + trace;
                throw ex;
            }
        }
 
        // Event during Upload process
        public void TransferPartProgressEvent(object sender, UploadProgressArgs e)
        {
            // Process event. 
            this.TransferredBytes = e.TransferredBytes;
            this.TotalBytes = e.TotalBytes;
            if (OnProgress != null) OnProgress(this,EventArgs.Empty);
        }

        // Event during Download process
        public void DownPartProgressEvent(object sender, WriteObjectProgressArgs e)
        {
            // Process event. 
            this.TransferredBytes = e.TransferredBytes;
            this.TotalBytes = e.TotalBytes;
            if (OnProgress != null) OnProgress(this,EventArgs.Empty);
        }

        // Upload Folder Content
        //
        public void TransferFolder(string DestinationFolder, string FromFolder, bool copySubDirs)
        {
           TransferFolder(MyAccessKeyID, MySecretKeyID, MyBucketName, DestinationFolder, FromFolder, copySubDirs);
        }
        private void TransferFolder(string AccessKeyID, string SecretKeyID, string bucketName, string DestinationFolder, string FromFolder, bool copySubDirs)
        {
            DirectoryInfo sourceDirectory = new DirectoryInfo(FromFolder);
            List<DirectoryInfo> sourceSubDirectories = FilterDirectories(sourceDirectory.GetDirectories(), new[] { "Scripting", ".svn" , ".", "RECYCLE"});
          
            FileInfo[] files = sourceDirectory.GetFiles();
            List<FileInfo> validFiles = FilterFiles(files, new[] { "._",".DS_Store","Thumbs.db" });

            try
            {
                foreach (FileInfo file in validFiles)
                {
                    string Filepath = Path.Combine(FromFolder, file.Name);
                    string keyName = DestinationFolder + "/" + file.Name;
                    this.TotalBytes = file.Length;
                    if (file.Length > 0) TransferFile(AccessKeyID, SecretKeyID, bucketName, keyName, Filepath);
                    this.AcumMb = this.AcumMb + this.TotalBytes;
                }

                if (!copySubDirs) return;

                foreach (DirectoryInfo subdir in sourceSubDirectories)
                {
                    string temppath = DestinationFolder + "/" + subdir.Name;
                    TransferFolder(AccessKeyID, SecretKeyID, bucketName, temppath, subdir.FullName, true);
                }
            }
            catch (AmazonS3Exception ex)
            {
                throw ex;
            }
        }

    // GetFolder Content including Subdirectories if exist
    //
    public void GetFolder(string FromFolder, string DestinationFolder)
    {
      GetFolder(MyAccessKeyID, MySecretKeyID, MyBucketName, FromFolder, DestinationFolder);
    }
    private void GetFolder(string AccessKeyID, string SecretKeyID, string bucketName, string FromFolder, string DestinationFolder)
    {
      try
      {
        Connect(AccessKeyID, SecretKeyID); // create S3client

        ListObjectsRequest request = new ListObjectsRequest();
        request = new ListObjectsRequest();
        request.BucketName = bucketName;
        request.WithPrefix(FromFolder);

        do
        {
          ListObjectsResponse response = S3client.ListObjects(request);
          // Process response.
          // Download the files Listed
          long n = response.S3Objects.Count;
          
              foreach (S3Object entry in response.S3Objects)
              {
                string directory = Path.GetDirectoryName(entry.Key);
                string Filename = Path.GetFileName(entry.Key);
                this.TotalBytes = entry.Size;

                string DestDirPath = DestinationFolder;

                 if (FromFolder != directory) // there are subdirectories
                 {
                    string[] dirlist = directory.Split('\\');
                    int i;  // begin at 1 to avoid first directory name = FromFolder
                    for (i = 1; i <= dirlist.Count() - 1; i++) // review all path and create subdirectory as needed
                    {
                        string subdir = dirlist[i];
                        DestDirPath = Path.Combine(DestinationFolder, subdir);
                        if (!Directory.Exists(DestDirPath))
                        Directory.CreateDirectory(DestDirPath);
                    }
                  }

                  // Dowload the file to the directory path
                  string Filepath = Path.Combine(DestDirPath, Filename);
                  DownLoadFile(AccessKeyID, SecretKeyID, bucketName, entry.Key, Filepath);
                  this.AcumMb = this.AcumMb + this.TotalBytes;
              }

          // If response is truncated, set the marker to get the next // set of keys. 
          if (response.IsTruncated) request.Marker = response.NextMarker;
          else request = null;

        }
        while (request != null);
      }

      catch (AmazonS3Exception ex)
      {
        throw ex;
      }

    }

    public void CopyObject(string FromKey)
    {
      CopyObject(MyAccessKeyID, MySecretKeyID, MyBucketName, FromKey);
    }
    private void CopyObject(string AccessKeyID, string SecretKeyID, string bucketName, string FromKey)
    {
        try
         { 
            Connect(AccessKeyID, SecretKeyID); // create S3client

            CopyObjectRequest request = new CopyObjectRequest();

            request.SourceBucket = bucketName;
            request.SourceKey = FromKey;
            request.DestinationBucket = "mediventabak";
            request.DestinationKey = FromKey;

            try
            {
              S3client.CopyObject(request);
              trace = "OK" ;
            }
             catch (AmazonS3Exception ex)
            {
                 trace = ex.Message;
            }

      }
        catch (AmazonS3Exception ex)
        {
            trace = ex.Message;
            throw ex;
        }
}

    // Copy Folder Content including Subdirectories if exist
    //
    public void CopyFolder(string FromFolder)
    {
      CopyFolder(MyAccessKeyID, MySecretKeyID, MyBucketName, FromFolder);
    }
    private void CopyFolder(string AccessKeyID, string SecretKeyID, string bucketName, string FromFolder)
    {
      try
      {
        Connect(AccessKeyID, SecretKeyID); // create S3client

        List<string> dirkeys = new List<string> { };

        ListObjectsRequest request = new ListObjectsRequest();
        request = new ListObjectsRequest();
        request.BucketName = bucketName;
        request.WithPrefix(FromFolder);

        do
        {
          ListObjectsResponse response = S3client.ListObjects(request);
          // Process response.
          // Download the files Listed
          long n = response.S3Objects.Count;


          foreach (S3Object entry in response.S3Objects)
          {
            string directory = Path.GetDirectoryName(entry.Key);
            string Filename = Path.GetFileName(entry.Key);
            this.TotalBytes = entry.Size;

            // Copy the Key;
            CopyObject(AccessKeyID, SecretKeyID, bucketName, entry.Key);
            this.AcumMb = this.AcumMb + this.TotalBytes;
          }


          // If response is truncated, set the marker to get the next // set of keys. 
          if (response.IsTruncated) request.Marker = response.NextMarker;
          else request = null;

        }
        while (request != null);
      }


      catch (AmazonS3Exception ex)
      {
        throw ex;
      }

    }


    // Delete Object keyName in bucketName
    //
    public void DeleteObject(string keyName)
        {
            DeleteObject(MyAccessKeyID, MySecretKeyID, MyBucketName, keyName);
        }
        private void DeleteObject(string AccessKeyID, string SecretKeyID, string bucketName, string keyName)
        {
            Connect(AccessKeyID, SecretKeyID); // create S3client

            DeleteObjectRequest Delrequest = new DeleteObjectRequest();
            Delrequest.WithBucketName(bucketName);
            Delrequest.WithKey(keyName);
            // Call request
            S3client.DeleteObject(Delrequest);
        }

        // Delete Folder Items 
        //
        public void DeleteFolderItems(string FromFolder)
        {
            DeleteFolderItems(MyAccessKeyID, MySecretKeyID, MyBucketName, FromFolder);
        }
        private void DeleteFolderItems(string AccessKeyID, string SecretKeyID, string bucketName, string FromFolder)
        {
            Connect(AccessKeyID, SecretKeyID); // create S3client

            ListObjectsRequest request = new ListObjectsRequest();
            request = new ListObjectsRequest();
            request.BucketName = bucketName;
            request.WithPrefix(FromFolder);

            do
            {
                ListObjectsResponse response = S3client.ListObjects(request);
                long n = response.S3Objects.Count;
                foreach (S3Object entry in response.S3Objects)
                {
                    DeleteObject(AccessKeyID, SecretKeyID, bucketName, entry.Key);
                }

                // If response is truncated, set the marker to get the next // set of keys. 
                if (response.IsTruncated) request.Marker = response.NextMarker;
                else request = null;
            } while (request != null);

            DeleteObject(AccessKeyID, SecretKeyID, bucketName, FromFolder); // try to delete folder at end.
        }
              
        // List Folder names in MyBucket
        //
        public List<string> GetFolderNames()
        {
            return GetFolderNames(MyAccessKeyID, MySecretKeyID, MyBucketName);
        }
        private List<string> GetFolderNames(string AccessKeyID, string SecretKeyID, string bucketName)
        {
            List<string> DirNames = new List<string>();
        
            Connect(AccessKeyID, SecretKeyID); // create S3client

            ListObjectsRequest request = new ListObjectsRequest();
            request = new ListObjectsRequest();
            request.BucketName = bucketName;
            request.WithPrefix("");

            string lastitem = null;
            int veces = 0;
            do
            {
                ListObjectsResponse response = S3client.ListObjects(request);     
                long n = response.S3Objects.Count;
                foreach (S3Object entry in response.S3Objects)
                {
                    string[] dirlist = entry.Key.Split('/');
                    if (dirlist[0] != lastitem) // do not include more than 1 time
                    {
                        DirNames.Add(dirlist[0]);
                        lastitem = dirlist[0];
                    }
                }

                // If response is truncated, set the marker to get the next // set of keys. 
                if (response.IsTruncated) request.Marker = response.NextMarker;
                else request = null;
              veces++;
            } while ((request != null) & (veces<10));

            return DirNames;
        }

        // List Files in Folder
        //
        public List<string> GetKeyNames(string FromFolder)
        {
            return GetKeyNames(MyAccessKeyID, MySecretKeyID, MyBucketName, FromFolder);
        }
        private List<string> GetKeyNames(string AccessKeyID, string SecretKeyID, string bucketName, string FromFolder)
        {
            try
            {
                List<string> FileList = new List<string>();

                Connect(AccessKeyID, SecretKeyID); // create S3client

                ListObjectsRequest request = new ListObjectsRequest();
                request = new ListObjectsRequest();
                request.BucketName = bucketName;
                request.WithPrefix(FromFolder);

                // Process response.
                ListObjectsResponse response = S3client.ListObjects(request);
                   
                // Add the files Listed
                   
                foreach (S3Object entry in response.S3Objects)
                {
                   string Filename = Path.GetFileName(entry.Key);
                   FileList.Add(Filename);                       
                }       
                return FileList;
            }

            catch (AmazonS3Exception ex)
            {
                throw ex;
            }

        }


        #region Helpers

        /// <summary>
        /// Filters the directory array by the non-wanted folders, and returns a list of applicable directories.
        /// </summary>
        /// <param name="subDirs">Array of subdirectories to be analyzed.</param>
        /// <param name="filterby">Array of values to filter the directory list by</param>
        /// <returns></returns>
        private static List<DirectoryInfo> FilterDirectories(IEnumerable<DirectoryInfo> subDirs, IEnumerable<string> filterby)
        {
            List<DirectoryInfo> directories = new List<DirectoryInfo>();
            foreach (DirectoryInfo eachSubDir in subDirs)
            {
                bool includeDir = true;
                foreach (string filterItem in filterby)
                {
                    if (eachSubDir.Name.Contains(filterItem))
                        includeDir = false;
                }
                if (includeDir)
                    directories.Add(eachSubDir);
            }
            return directories;
        }
      
        /// <summary>
        /// Filters the directory array by the non-wanted folders, and returns a list of applicable directories.
        /// </summary>
        /// <param name="allFiles">Files to be analized</param>
        /// <param name="filterby">Array of values to filter the directory list by</param>
        /// <returns></returns>
        private static List<FileInfo> FilterFiles(IEnumerable<FileInfo> allFiles, IEnumerable<string> filterby)
        {
            List<FileInfo> filteredFiles = new List<FileInfo>();
            foreach (FileInfo eachFile in allFiles)
            {
                bool includeFile = true;
                foreach (string filterItem in filterby)
                {
                    if (eachFile.Name.Contains(filterItem))
                        includeFile = false;
                }
                if (includeFile)
                    filteredFiles.Add(eachFile);
            }
            return filteredFiles;
        }

        #endregion
    }
}
