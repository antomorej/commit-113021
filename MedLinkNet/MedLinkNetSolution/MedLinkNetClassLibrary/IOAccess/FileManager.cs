﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MedLinkNet.IOAccess
{
    /// <summary>
    /// Defined commom I/O operations.
    /// </summary>
    public class FileManager
    {
        public static string LastFileSend;
        /// <summary>
        /// Store the last file path copied in CopyDirectory execution
        /// </summary>
        public static List<string> GetDirectories(string workingDir)
        {
            return Directory.GetDirectories(workingDir).ToList();
        }

        public static bool ValidateDirectory(string path)
        {
            return Directory.Exists(path);
        }

        public static List<string> GetDirectoriesName(string workingDir)
        {
            List<string> dirs = Directory.GetDirectories(workingDir).ToList();
            List<string> dirNames = new List<string>();
            foreach (var dir in dirs)
            {
                string[] allPieces = dir.Split(new[] { "\\" }, StringSplitOptions.None);
                dirNames.Add(allPieces[allPieces.Length - 1]);
            }
            return dirNames;
        }

        /// <summary>
        /// Copies a directory to the specified location. Example call: DirectoryCopy(".", @".\temp", true);
        /// </summary>
        /// <param name="sourceFile">The location of the directory that is to be copied.</param>
        /// <param name="destDirName">The destination where the source it to be copied.</param>
        public static void CopyFile(string sourceFile, string destDirName)
        {          
            if (!Directory.Exists(destDirName))
                Directory.CreateDirectory(destDirName);

            FileInfo file = new FileInfo(sourceFile);
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
                LastFileSend = temppath;
            }
        }

        /// <summary>
        /// Copies a directory to the specified location. Example call: DirectoryCopy(".", @".\temp", true);
        /// </summary>
        /// <param name="sourceDirName">The location of the directory that is to be copied.</param>
        /// <param name="destDirName">The destination where the source it to be copied.</param>
        public static void CopyDirectory(string sourceDirName, string destDirName)
        {
            DirectoryInfo sourceDirectory = new DirectoryInfo(sourceDirName);
            ValidateDirectory(sourceDirName, sourceDirectory);

            if (!Directory.Exists(destDirName))
                Directory.CreateDirectory(destDirName);

            FileInfo[] files = sourceDirectory.GetFiles();
            List<FileInfo> validFiles = FilterFiles(files, new[] { "._.Trashes",".DS_Store","Thumbs.db" });
            foreach (FileInfo file in validFiles)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
                LastFileSend = temppath;
            }
        }

        /// <summary>
        /// Copies a directory to the specified location. Example call: DirectoryCopy(".", @".\temp", true);
        /// </summary>
        /// <param name="sourceDirName">The location of the directory that is to be copied.</param>
        /// <param name="destDirName">The destination where the source it to be copied.</param>
        /// <param name="copySubDirs">Determines if subdirectories of the souce will also be copied to the new location.</param>
        public static void CopyDirectory(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo sourceDirectory = new DirectoryInfo(sourceDirName);
            ValidateDirectory(sourceDirName, sourceDirectory);

            List<DirectoryInfo> sourceSubDirectories = FilterDirectories(sourceDirectory.GetDirectories(), new[] { "Scripting", ".svn", ".", "RECYCLE" });
            if (!Directory.Exists(destDirName))
                Directory.CreateDirectory(destDirName);

            FileInfo[] files = sourceDirectory.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            if (!copySubDirs) return;
            foreach (DirectoryInfo subdir in sourceSubDirectories)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                CopyDirectory(subdir.FullName, temppath, true);
            }
        }

        /// <summary>
        /// Deletes the content of the specified directory path, and subdirectories if needed.
        /// </summary>
        /// <param name="sourceDirName">Source directory path.</param>
        /// <param name="deleteSubDirs">Determines whether to delete subdirectories of the source.</param>
        public static void ClearDirectory(string sourceDirName, bool deleteSubDirs)
        {
            DirectoryInfo sourceDirectory = new DirectoryInfo(sourceDirName);
            ValidateDirectory(sourceDirName, sourceDirectory);

            List<DirectoryInfo> subDirs = FilterDirectories(sourceDirectory.GetDirectories(), new[] { "Scripting", ".svn" });
            if (subDirs.Count > 0)
            {
                FileInfo[] subFiles = sourceDirectory.GetFiles();

                foreach (DirectoryInfo subDir in subDirs)
                    ClearDirectory(subDir.FullName, true);
                foreach (FileInfo subFile in subFiles)
                    subFile.Delete();
            }
        }

        /// <summary>
        /// Validates a given directory path and throws an exception if invalid.
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="sourceDirectory"></param>
        public static void ValidateDirectory(string sourceDirName, DirectoryInfo sourceDirectory)
        {
            if (!sourceDirectory.Exists)
                throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
        }

        public static List<string> ReadFileAndFilterLines(string filePath, string[] filterby)
        {
            List<string> lines = new List<string>();
            StreamReader streamReader = File.OpenText(filePath);
            using (streamReader)
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    bool addLine = true;
                    foreach (string filterItem in filterby)
                    {
                        if (line.Contains(filterItem))
                            addLine = false;
                    }
                    if (addLine)
                        lines.Add(line);
                }
            }
            streamReader.Close();
            return lines;
        }

        public static void Append(string filePath, List<string> linesToAppend)
        {
            StreamWriter streamWriter = File.AppendText(filePath);
            foreach (string lineToAppend in linesToAppend)
                streamWriter.WriteLine(lineToAppend);

            streamWriter.Close();
        }

        /// <summary>
        /// Clears the file at the given path.
        /// </summary>
        /// <param name="path">Path where the file is located</param>
        public static void Clear(string path)
        {
            Write(new[] { "" }, path);
        }

        /// <summary>
        /// Writes the given lines to the given path, overriding existing information
        /// </summary>
        /// <param name="lines">Lines to be writen</param>
        /// <param name="path">Path where the file is located</param>
        public static void Write(string[] lines, string path)
        {
            StreamWriter writer = new StreamWriter(path, false);
            using (writer)
            {
                foreach (string line in lines)
                {
                    writer.WriteLine(line);
                }
            }
            writer.Close();
        }

        /// <summary>
        /// Gets the list of file names in a given directory.
        /// </summary>
        /// <param name="path">Path to look for</param>
        /// <param name="extensions">Extentions of files to retrieve</param>
        /// <param name="option">Search Option mode</param>
        /// <returns></returns>
        public static string[] GetFilesByExtensions(string path, string extensions, SearchOption option)
        {
            List<string> result = new List<string>();
            InternalGetFiles(path, option, extensions.Split(';'), result);
            return result.ToArray();
        }

        #region Helpers

        /// <summary>
        /// Filters the directory array by the non-wanted folders, and returns a list of applicable directories.
        /// </summary>
        /// <param name="subDirs">Array of subdirectories to be analyzed.</param>
        /// <param name="filterby">Array of values to filter the directory list by</param>
        /// <returns></returns>
        private static List<DirectoryInfo> FilterDirectories(IEnumerable<DirectoryInfo> subDirs, IEnumerable<string> filterby)
        {
            List<DirectoryInfo> directories = new List<DirectoryInfo>();
            foreach (DirectoryInfo eachSubDir in subDirs)
            {
                bool includeDir = true;
                foreach (string filterItem in filterby)
                {
                    if (eachSubDir.Name.Equals(filterItem))
                        includeDir = false;
                }
                if (includeDir)
                    directories.Add(eachSubDir);
            }
            return directories;
        }

        /// <summary>
        /// Filters the directory array by the non-wanted folders, and returns a list of applicable directories.
        /// </summary>
        /// <param name="allFiles">Files to be analized</param>
        /// <param name="filterby">Array of values to filter the directory list by</param>
        /// <returns></returns>
        private static List<FileInfo> FilterFiles(IEnumerable<FileInfo> allFiles, IEnumerable<string> filterby)
        {
            List<FileInfo> filteredFiles = new List<FileInfo>();
            foreach (FileInfo eachFile in allFiles)
            {
                bool includeFile = true;
                foreach (string filterItem in filterby)
                {
                    if (eachFile.Name.Contains(filterItem))
                        includeFile = false;
                }
                if (includeFile)
                    filteredFiles.Add(eachFile);
            }
            return filteredFiles;
        }

        private static void InternalGetFiles(string path, SearchOption option, string[] extensions, List<string> result)
        {
            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                string ext = Path.GetExtension(file);
                foreach (string p in extensions)
                {
                    if (ext == p)
                    {
                        result.Add(file);
                        break;
                    }
                }
            }

            if (option == SearchOption.TopDirectoryOnly)
                return;
            foreach (string directory in Directory.GetDirectories(path))
            {
                InternalGetFiles(directory, option, extensions, result);
            }
        }

        #endregion
    }
}
