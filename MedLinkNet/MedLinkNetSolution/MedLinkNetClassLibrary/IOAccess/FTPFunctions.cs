﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace MedLinkNet.IOAccess
{
    public class FTPFunctions
    {
       string username = MedLinkNet.Properties.Settings.Default.FTPUsername;
       string password = MedLinkNet.Properties.Settings.Default.FTPPassw;
       string ftppath = MedLinkNet.Properties.Settings.Default.FTPPath;

        public string trace;

        public void Upload(string localfile)
        {
            try
            {
               
                FileInfo finfo = new FileInfo(localfile); // tomar el nombre del file
                string name = finfo.Name;
                string url = ftppath + name; // mismo name en la url
               

                // Get the object used to communicate with the server.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential(username,password);

                // Copy the contents of the file to the request stream.
                StreamReader sourceStream = new StreamReader(localfile);
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);

                response.Close();  

            }
            catch (WebException ex)
            {
                trace = "EX" + trace;
                throw ex; 
            }
            
        }

    }
}
