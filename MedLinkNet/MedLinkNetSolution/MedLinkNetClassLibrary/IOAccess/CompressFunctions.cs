﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.IO.Compression;

namespace MedLinkNet.IOAccess
{
    public static class CompressFunctions
    {

        public static string CompressExt = ".cmp";
   
        public static void Compress(string fromfile, string tofile, bool AddExt)
        {
          FileInfo fileToCompress = new FileInfo(fromfile);
          if (AddExt) tofile = tofile + CompressExt;
          using (FileStream originalFileStream = fileToCompress.OpenRead())
          {
                using (FileStream compressedFileStream = File.Create(tofile))
                {
                    using (DeflateStream compressionStream = new DeflateStream(compressedFileStream, CompressionMode.Compress))
                    {
                        originalFileStream.CopyTo(compressionStream);
                    }
                }
          }
        }

        public static void Decompress(string fromfile, string tofile)
        {
          FileInfo fileToDecompress = new FileInfo(fromfile);
          using (FileStream originalFileStream = fileToDecompress.OpenRead())
          {
            using (FileStream decompressedFileStream = File.Create(tofile))
        	{
                using (DeflateStream decompressionStream = new DeflateStream(originalFileStream, CompressionMode.Decompress))
        	    {
                    decompressionStream.CopyTo(decompressedFileStream);
        	    }
        	}
          }
        }

    }

}
