﻿CREATE VIEW [dbo].[UserView]
AS
SELECT     dbo.USERS.USERID, dbo.DOMINIOS.IDDOMINIO, DOMINIOS.DESCRIPTION AS DOMINIONAME,  dbo.WORKGROUP.IDWORKGROUP, dbo.WORKGROUP.DESCRIPTION AS GROUPNAME,  dbo.TESTID.TESTID, dbo.TEST.TESTNAME, dbo.USERS.NOMBRE, dbo.USERS.USERNAME, dbo.USERS.PASSWORD, dbo.USERS.ENVIAA,   
                      dbo.USERS.IDUSERTYPE, dbo.USERTYPES.USERTYPE, dbo.USERS.ENABLED, dbo.USERS.LASTTIME,
                      dbo.WORKGROUP.JDENABLED, dbo.WORKGROUP.DISKNAME, 
                      dbo.WORKGROUP.AWSENABLED
                      
FROM         dbo.USERS INNER JOIN
                      dbo.WORKGROUP ON dbo.USERS.IDWORKGROUP = dbo.WORKGROUP.IDWORKGROUP INNER JOIN
					  dbo.USERTYPES ON dbo.USERTYPES.IDTEST = dbo.USERS.IDUSERTYPE INNER JOIN
                      dbo.TEST ON dbo.WORKGROUP.IDTEST = dbo.TEST.TESTID INNER JOIN
                      dbo.DOMINIOS ON dbo.WORKGROUP.IDDOMINIO = dbo.DOMINIOS.IDDOMINIO
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "USERS"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 213
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "WORKGROUP"
            Begin Extent = 
               Top = 6
               Left = 251
               Bottom = 135
               Right = 424
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TEST"
            Begin Extent = 
               Top = 120
               Left = 484
               Bottom = 215
               Right = 654
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DOMINIOS"
            Begin Extent = 
               Top = 0
               Left = 480
               Bottom = 112
               Right = 655
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   En', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'UserView';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'd
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'UserView';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'UserView';

